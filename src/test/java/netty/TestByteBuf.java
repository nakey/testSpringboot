package netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.junit.Test;

import java.util.Arrays;

/**
 * @ClassName TestByteBuf
 * @Description 测试ByteBuf的使用
 *  原文链接：https://blog.csdn.net/XinhuaShuDiao/article/details/102521496
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2021/11/17 14:38
 */
public class TestByteBuf {

    @Test
    public void TestUser(){
        // 1.创建一个非池化的ByteBuf，大小为10个字节
        ByteBuf buf = Unpooled.buffer(10);

        System.out.println("原始ByteBuf为====================>" + buf.toString());
        System.out.println("1.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("----------------------------------2-------------------------------");
        // 2.写入一段内容
        byte[] bytes = {1, 2, 3, 4, 5};
        buf.writeBytes(bytes);
        System.out.println("写入的bytes为====================>" + Arrays.toString(bytes));
        System.out.println("写入一段内容后ByteBuf为===========>" + buf.toString());
        System.out.println("2.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("写入之后的写指针位置为:"+buf.writerIndex());
        System.out.println("----------------------------------3-------------------------------");
        // 3.读取一段内容
        System.out.println("读之前的读指针位置为:"+buf.readerIndex());
        byte b1 = buf.readByte();
        byte b2 = buf.readByte();
        System.out.println("读取的bytes为====================>" + Arrays.toString(new byte[]{b1, b2}));
        System.out.println("读取一段内容后ByteBuf为===========>" + buf.toString());
        System.out.println("3.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("读之后的读指针位置为:"+buf.readerIndex());
        System.out.println("读之后的写指针位置为:"+buf.writerIndex());
        System.out.println("----------------------------------4-------------------------------");
        // 4.将读取的内容丢弃
        buf.discardReadBytes();
        System.out.println("将读取的内容丢弃后ByteBuf为========>" + buf.toString());
        System.out.println("4.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("丢弃之后的读指针位置为:"+buf.readerIndex());
        System.out.println("丢弃之后的写指针位置为:"+buf.writerIndex());
        System.out.println("----------------------------------5-------------------------------");
        // 5.清空读写指针
        buf.clear();
        System.out.println("将读写指针清空后ByteBuf为==========>" + buf.toString());
        System.out.println("5.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("清空之后的读指针位置为:"+buf.readerIndex());
        System.out.println("清空之后的写指针位置为:"+buf.writerIndex());
        System.out.println("----------------------------------6-------------------------------");
        // 6.再次写入一段内容，比第一段内容少
        byte[] bytes2 = {1, 2, 3};
        buf.writeBytes(bytes2);
        System.out.println("写入的bytes为====================>" + Arrays.toString(bytes2));
        System.out.println("写入一段内容后ByteBuf为===========>" + buf.toString());
        System.out.println("6.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("再次写入之后的读指针位置为:"+buf.readerIndex());
        System.out.println("再次写入之后的写指针位置为:"+buf.writerIndex());
        //在第四个位置插入一个8
        byte insert = 8;
        buf.writeByte(insert);
        System.out.println("ByteBuf第四个位置插入8后的内容为===============>" + Arrays.toString(buf.array()) + "\n");

        System.out.println("----------------------------------7-------------------------------");
        // 7.将ByteBuf清零
        buf.setZero(0, buf.capacity());
        System.out.println("将内容清零后ByteBuf为==============>" + buf.toString());
        System.out.println("7.ByteBuf中的内容为================>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("清零之后的读指针位置为:"+buf.readerIndex());
        System.out.println("清零之后的写指针位置为:"+buf.writerIndex());
        System.out.println("----------------------------------8-------------------------------");
        // 8.再次写入一段超过容量的内容
        byte[] bytes3 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        buf.writeBytes(bytes3);
        System.out.println("写入的bytes为====================>" + Arrays.toString(bytes3));
        System.out.println("写入一段内容后ByteBuf为===========>" + buf.toString());
        System.out.println("8.ByteBuf中的内容为===============>" + Arrays.toString(buf.array()) + "\n");
        System.out.println("最后读指针位置为:"+buf.readerIndex());
        System.out.println("最后写指针位置为:"+buf.writerIndex());

    }
}
