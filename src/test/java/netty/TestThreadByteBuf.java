package netty;

import com.xiechy.util.CalculateHours;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.lf5.viewer.LogTable;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * @ClassName TestThreadByteBuf
 * @Description 测试多线程下的ByteBuf是否线程安全
 *
 * 其中一次的结果为：
 * 编号1开始写入...
 * 线程1开始读!
 * 编号2开始写入...
 * 线程2开始读!
 * 2读之前的读指针位置为:0
 * 1读之前的读指针位置为:0
 * 编号3开始写入...
 * 编号1写完...
 * 编号3写完...
 * 编号2写完...
 * 3写入ByteBuf中的内容为=3==============>[3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *
 * 2写入ByteBuf中的内容为=2==============>[3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *
 * 1写入ByteBuf中的内容为=1==============>[3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *
 * 线程3开始读!
 * 编号4开始写入...
 * 编号4写完...
 * 4写入ByteBuf中的内容为=4==============>[3, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 *
 * 线程5开始读!
 * 5读之前的读指针位置为:0
 * 3读之前的读指针位置为:0
 *
 *
 *
 * 证明netty里面的ByteBuf不是线程安全的
 * 咨询其他人：不是线程安全的
 *
 *
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2021/11/17 14:57
 */
@Slf4j
public class TestThreadByteBuf {

    /**
     * @Description 测试同时读写
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/11/17 16:27
     * @param
     * @return void
    */
    @Test
    public void testReadWrite() {
        ByteBuf byteBuf = Unpooled.buffer(20);
        for (byte i = 1; i < 11; i++) {
            WriteThread w = new WriteThread(byteBuf, i);
            w.start();
            Readthread r = new Readthread(byteBuf, i);
            r.start();
            //如果改成run的方式就是按顺序执行，不会有什么大问题
           /* WriteThread w = new WriteThread(byteBuf, i);
            w.run();
            Readthread r = new Readthread(byteBuf, i);
            r.run();*/
        }
    }
}


class WriteThread extends  Thread{

    private ByteBuf byteBuf;

    private byte index;

    public WriteThread(ByteBuf byteBuf, byte index){
        this.byteBuf = byteBuf;
        this.index = index;
    }

    @Override
    public void run() {
        System.out.println("编号"+index+"开始写入...");
        byte[] bytes = {index};
        byteBuf.writeBytes(bytes);
        System.out.println("编号"+index+"写完...");
        System.out.println(index+"写入ByteBuf中的内容为="+index+"==============>" + Arrays.toString(byteBuf.array()) + "\n");
    }
}


class Readthread extends  Thread{

    private static final Logger log = LoggerFactory.getLogger(Readthread.class);

    private ByteBuf byteBuf;

    private byte index;

    public Readthread(ByteBuf byteBuf, byte index){
        this.byteBuf = byteBuf;
        this.index = index;
    }

    @Override
    public void run() {
        System.out.println("线程" + index + "开始读!");
        System.out.println(index+"读之前的读指针位置为:" + byteBuf.readerIndex());
        byte b1 = 111;
        try {
            b1 = byteBuf.readByte();
        } catch (Exception e) {
            log.error("异常", e);
            System.out.println("读取异常!");
        }
        System.out.println(index+"读取的byte为====================>" + Arrays.toString(new byte[]{b1}));
        System.out.println(index+"读取一段内容后ByteBuf为===========>" + byteBuf.toString());
    }

    /**
     * @Description 这是另外一次测试结果
     *
     * 编号1开始写入...
     * 线程1开始读!
     * 编号2开始写入...
     * 1读之前的读指针位置为:0
     * 编号1写完...
     * 线程2开始读!
     * 2读之前的读指针位置为:0
     * 1写入ByteBuf中的内容为=1==============>[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 编号2写完...
     * 编号4开始写入...
     * 编号4写完...
     * 2写入ByteBuf中的内容为=2==============>[1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 线程4开始读!
     * 4写入ByteBuf中的内容为=4==============>[1, 2, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 4读之前的读指针位置为:0
     * 编号3开始写入...
     * 编号3写完...
     * 3写入ByteBuf中的内容为=3==============>[1, 2, 4, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 编号6开始写入...
     * 编号6写完...
     * 6写入ByteBuf中的内容为=6==============>[1, 2, 4, 3, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 线程3开始读!
     * 3读之前的读指针位置为:0
     * 编号5开始写入...
     * 编号5写完...
     * 5写入ByteBuf中的内容为=5==============>[1, 2, 4, 3, 6, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 线程5开始读!
     * 4读取的byte为====================>[1]
     * 2读取的byte为====================>[1]
     * 3读取的byte为====================>[1]
     * 4读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 1, widx: 6, cap: 20)
     * 1读取的byte为====================>[1]
     * 编号7开始写入...
     * 线程6开始读!
     * 6读之前的读指针位置为:1
     * 编号7写完...
     * 1读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 1, widx: 6, cap: 20)
     * 2读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 1, widx: 6, cap: 20)
     * 3读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 1, widx: 6, cap: 20)
     * 编号8开始写入...
     * 5读之前的读指针位置为:1
     * 编号8写完...
     * 8写入ByteBuf中的内容为=8==============>[1, 2, 4, 3, 6, 5, 7, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 线程10开始读!
     * 编号10开始写入...
     * 编号10写完...
     * 10写入ByteBuf中的内容为=10==============>[1, 2, 4, 3, 6, 5, 7, 8, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 7写入ByteBuf中的内容为=7==============>[1, 2, 4, 3, 6, 5, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     * 6读取的byte为====================>[2]
     * 6读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 3, widx: 9, cap: 20)
     * 线程8开始读!
     * 8读之前的读指针位置为:3
     * 线程9开始读!
     * 9读之前的读指针位置为:4
     * 9读取的byte为====================>[6]
     * 9读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 5, widx: 9, cap: 20)
     * 8读取的byte为====================>[3]
     * 10读之前的读指针位置为:3
     * 10读取的byte为====================>[5]
     * 10读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 6, widx: 9, cap: 20)
     * 5读取的byte为====================>[4]
     * 5读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 6, widx: 9, cap: 20)
     * 8读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 5, widx: 9, cap: 20)
     * 线程7开始读!
     * 7读之前的读指针位置为:6
     * 编号9开始写入...
     * 编号9写完...
     * 7读取的byte为====================>[7]
     * 7读取一段内容后ByteBuf为===========>UnpooledByteBufAllocator$InstrumentedUnpooledUnsafeHeapByteBuf(ridx: 7, widx: 10, cap: 20)
     * 9写入ByteBuf中的内容为=9==============>[1, 2, 4, 3, 6, 5, 7, 8, 10, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     *
     *
    */
}
