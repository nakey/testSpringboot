package com.xiechy.javaBase;

import cn.hutool.core.util.RandomUtil;
import com.xiechy.wx.bean.User;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class TestStatic {

	public static void funA() {
		TestStatic t = new TestStatic();
		t.funB();
	}

	public void funB() {
		System.out.println("实例方法！");
	}


	/**
	 * @Description 测试静态方法传入的非静态变量user是不是线程共享的
	 * 结论：传进去方法的引用变量，得看这个变量是从哪里来，是否为线程共享，如果是，那么就线程不安全，如果每个线程独享，那么线程安全
	 * @Author R&D-VAL SZ nakey.xie
	 * @Date  2021/11/9 9:57
	 * @param user
	 * @return com.xiechy.wx.bean.User
	*/
	public static User test(User user) throws InterruptedException {
		//第一个线程进来设置age为18,第二个线程会不会跑到else里面?
		if (user.getAge() == null) {
			user.setAge(18);
		} else {
			log.info("进入else....");
			user.setImg("56");
		}
		if (RandomUtil.randomBoolean()) {
			//假设第一个线程到这里睡眠了
			log.info("准备睡眠...");
			Thread.sleep(2000L);
		}
		return user;
	}

	public static void main(String[] args) {
		User u = new User();
		for (int i = 0; i < 20; i++) {
			MyTestThread t = new MyTestThread(u);
			t.start();
		}

	}
	}

class MyTestThread extends Thread {

	public MyTestThread(User u) {
		this.u = u;
	}

	private User u;

	@Override
	public void run() {
		User u = null;
		try {
			u = TestStatic.test(this.u);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (u == null) {
			System.out.println("user is null");
		} else {
			System.out.println("age:" + u.getAge());
			System.out.println("img:" + u.getImg());
		}
	}
}


	abstract class TestAbstruct {
		public void fun1() {
		}

		abstract void fun2();

	}


    //公共类型的接口只能定义在自己的原文件中
/*public interface InterfaceA{
	a();
}*/

