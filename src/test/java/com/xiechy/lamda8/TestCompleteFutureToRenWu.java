package com.xiechy.lamda8;

import com.xiechy.entity.TableEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @Title TestCompleteFutureToRenWu
 * @Description 使用completefutrue处理任务
 * @Program testspringboot
 * @Author nakey.xie
 * @Version 1.0.0
 * @Date 2023/01/30
 */
public class TestCompleteFutureToRenWu {

    static CompletableFuture<List<TableEntity>> testTask(String id, ThreadPoolTaskExecutor executor) {
        return CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + ":start!");
            System.out.println("正在处理：" + id);
            //测试异常情况不需要返回
            if("5".equals(id)){
                return null;
            }
            if("15".equals(id)){
                return null;
            }
           /* try {
                //模拟耗时操作
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("睡过头了!");
            }*/
            System.out.println(Thread.currentThread().getName() + ":end!");
            List<TableEntity> entities = new ArrayList<>();

            TableEntity entity = new TableEntity();
            entity.setClassName(id);
            TableEntity entity2 = new TableEntity();
            entity2.setComments(id);
            entities.add(entity);
            entities.add(entity2);
            return entities;
        }, executor);
    }

    public static void batchProcess(List<String> taskList, ThreadPoolTaskExecutor executor) {
        // 并行执行任务
        List<CompletableFuture<List<TableEntity>>> accountFindingFutureList =
                taskList.stream().map(task -> testTask(task, executor)).collect(Collectors.toList());

        // 使用allOf方法来表示所有的并行任务
        CompletableFuture<Void> allFutures =
                CompletableFuture
                        .allOf(accountFindingFutureList.toArray(new CompletableFuture[accountFindingFutureList.size()]));
        //allFutures.join();

        // 下面的方法可以帮助我们获得所有子任务的处理结果
        CompletableFuture<List<List<TableEntity>>> finalResults = allFutures.thenApply(v -> accountFindingFutureList.stream().map(accountFindingFuture -> accountFindingFuture.join())
                .collect(Collectors.toList()));

        try {
            List<List<TableEntity>> lists = finalResults.get();
            System.out.println(lists);
            System.out.println("处理完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        //线程池执行
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // 核心线程数2
        executor.setCorePoolSize(5);
        //最大线程数4
        executor.setMaxPoolSize(10);
        //队列最大数
        executor.setQueueCapacity(1500);
        //最大存活时间
        executor.setKeepAliveSeconds(5);

        //Thread.currentThread().getName()
        executor.setThreadNamePrefix("asyncMinExecutor-");
        //AbortPolicy - 抛出异常，中止任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        // 这一步千万不能忘了，否则报错： java.lang.IllegalStateException: ThreadPoolTaskExecutor not initialized
        executor.initialize();

        //模拟990个任务
        List<String> taskList = new ArrayList<>();
        for(int i=0;i<10; i++){
            taskList.add(i+"");
        }
        batchProcess(taskList, executor);
        executor.shutdown();
    }
}