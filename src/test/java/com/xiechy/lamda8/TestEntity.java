package com.xiechy.lamda8;

import lombok.Data;

/**
 * @author PC
 * @title: wangxing
 * @projectName nacosProjects
 * @description: TODO
 * @date 2021/11/39:24
 */
@Data
public class TestEntity {
    private String name;
    private String age;
    private String phone;
    private String gender;

    public TestEntity() {
    }

    public TestEntity(String name, String age, String phone, String gender) {
        this.age = age;
        this.name = name;
        this.phone = phone;
        this.gender = gender;
    }

}
