package com.xiechy.lamda8;

/**
 * @ClassName UseInteface
 * @Description TODO
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2022/2/18 16:51
 */
public class UseInteface {

    @FunctionalInterface
    interface Animal2{
        void say();
    }

    public static void main(String[] args) {
        //需要实现种动物叫

        //接口形式：
        Ainmal a = new Dog();
        animalSay(a);

        //function形式
        Animal2 animal2 = ()-> System.out.println("狗");
        animal2.say();
    }

    private static void animalSay(Ainmal a) {
        a.say();
    }


}
