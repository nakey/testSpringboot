package com.xiechy.lamda8;

import com.xiechy.lamda8.TestEntity;
import org.apache.commons.lang3.StringUtils;



import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author PC
 * @title: wangxing
 * @projectName nacosProjects
 * @description: TODO
 * @date 2021/10/2914:16
 */
public class TestJDK8 {




    public static void jdk8_list() {
        List<String> list = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        list.add("ss");
        list.add("aa");
        list.add("cc");
        list.add("vv");
        list.add("bb");
        list.add("nn");
        list.add("kk");

        List<String> stringList = Arrays.asList("小明", "小强", "", "小芳", "小小", "");

        list.stream().filter(str -> str != null && str.equals("aa")).forEach(s -> list2.add(s));//去重 添加list集合
        list.stream().filter(str -> !str.contains("aa")).collect(Collectors.toList());//去重返回list
        System.out.println(list2.size());
        //一个list集合内有name，sex字段，需要判断list中是否有name有叫张三的人，如果有返回true
        boolean c=  stringList.stream().filter(m->m.equals("小强")).findAny().isPresent();
        boolean b= stringList.stream().anyMatch(a -> a.equals("小小"));
        System.out.println("*******************b:"+b);
        System.out.println("*******************c:"+c);
        String str = list.stream().filter(s -> !s.equals("aa") && !s.equals("bbb")).collect(Collectors.joining(","));
        System.out.println(str);
        Long count = stringList.stream().filter(string -> string.isEmpty()).count();
        System.out.println(count);

    }

    public static void testJdk8Count() {
        List<TestEntity> list = new ArrayList<>();
        list.add(new TestEntity("wangxing", "21", "110", "nan"));
        list.add(new TestEntity("wangxing", "21", "110", "nan"));
        Double d = list.stream().mapToDouble(item -> Double.parseDouble(item.getAge())).sum();
        Long aa = list.stream().collect(Collectors.summingLong(testEntity -> new Long(testEntity.getAge())));
        System.out.println(aa);
        System.out.println(d);
        //直接去重
        List<TestEntity> testEntities = list.stream().distinct().collect(Collectors.toList());
        System.out.println(testEntities.size());
        // 多个条件去重
        List<TestEntity> testEntities1 =
                list.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(TestEntity -> TestEntity.getName()+";"+TestEntity.getAge()))), ArrayList::new));
        testEntities.forEach(item->{
            System.out.println(item.getAge());
        });
        System.out.printf("list size: %s \n",testEntities.size());
        //条件去重 方法2
        list.stream().collect(Collectors.toMap(TestEntity -> TestEntity.getName()+";"+TestEntity.getAge(), Function.identity(),(oldValue,newValue)->oldValue))
                .values().stream().forEach(System.out::println);


    }


    /**
     *  //TODO https://blog.csdn.net/zjhred/article/details/84976734
     * Optional  代替if else 写法
     * @throws Exception
     */
    public static void jdk8Optional(){
        List<String> stringList = Arrays.asList("小明", "小强", "", "小芳", "小小", "",null);
        /**
         * 判断空返回异常
         */
        Optional.ofNullable(stringList.get(1)).orElseThrow(() ->new RuntimeException("数据不能为空"));
        /**
         * 判空并做一些逻辑处理
         *
         * 等价于
         * if(user!=null){
         *     dosomething(user);
         * }
         *h
         *
         * Optional.ofNullable(变量).orElse(默认数值);
         *
         *
         *
         // 从前台界面获取输入的用户信息
         String user = getUserFromUI();
         if (user == null){
         return 0;
         } else {
         return user.length();

         使用JDK8中的Optional实现方式：

         // 从前台界面获取输入的用户信息
         String user = getUserFromUI();
         return Optional.ofNullable(user).orElse("").length;
         *
         */
        Optional.ofNullable(stringList.get(2)).ifPresent(s -> {
            //空串不是null,所以可以输出
            System.out.println("第三个为空字符串，不是null");
        });

        Optional.ofNullable(stringList.get(6)).ifPresent(s->{
            System.out.println("第六个为空!,不会输出到控制台!");
        });

        /*
         * 来源：判断空简化
         */

        List<String> list = new ArrayList<>(Arrays.asList(stringList.get(1), stringList.get(2), stringList.get(3), stringList.get(4), stringList.get(5), "Java技术栈（默认）"));
        //判空删除
        list.removeIf(StringUtils::isBlank);
        String result = list.get(0);
        Optional<List<String>> optionList = Optional.ofNullable(stringList);
        //optionList.map(String::toString).orElse( new String());

        TestEntity testEntity = null;


    }

    public static void JDK8_stream(){

        List<String> list =  Arrays.asList("啊强","啊明","啊芳","鬼脚7");
        // 使用Collection下的 stream() 和 parallelStream() 方法
        Stream<String> stream = list.stream(); //获取一个顺序流
        Stream<String> parallelStream = list.parallelStream(); //获取一个并行流
        //顺序流 of()
        Stream<Integer> streamInt = Stream.of(6, 4, 6, 7, 3, 9, 8, 10, 12, 14, 14);
        //过滤 filter()
        Stream<Integer> newStream = streamInt.filter(s -> s > 5) //6 6 7 9 8 10 12 14 14
                //distinct：通过流中元素的 hashCode() 和 equals() 去除重复元素
                .distinct() //6 7 9 8 10 12 14
                .skip(2) //9 8 10 12 14
                .limit(2); //9 8
        newStream.forEach(System.out::println);

    }

    public  static void JDK8_BigDecimal(){
        List<BigDecimal> list = Arrays.asList(new BigDecimal("0.88"),new BigDecimal("8.2"));
        //BigDecimal某一列相加
        BigDecimal amount = list.stream().map(m->m).reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println("ssssssssssss:"+amount);
        //BigDecimal某两列相乘后相加
        // BigDecimal amount2 = list.stream().map(Object-> Object.getInit_sale_price().multiply(new BigDecimal(Object.getBuyyer_count()))).reduce(BigDecimal.ZERO, BigDecimal::add);

    }

    public static void main(String[] args) {
        jdk8Optional();

        //double f=13.8;
        //System.out.println(13.8/3);


//        test1();
        // testJdk8Count();
        // jdk8_list();
//        JDK8_BigDecimal();
//        Map<String,String> map = new HashMap<>();
//        map.put("a","bssss");
//        map.put("b","bssss");
//        for (Map.Entry<String,String> stringEntry : map.entrySet()){
//            int a = stringEntry.getKey().lastIndexOf("~~~");
//            System.out.println(stringEntry.getKey().lastIndexOf("a"));
//        }

        //System.out.println(String.valueOf(null));
        //System.out.println(testFunction(4,i -> i * 2 + 1));
    }

    public static int testFunction(int i, Function<Integer,Integer> function) {
        return function.apply(i);
    }
}
