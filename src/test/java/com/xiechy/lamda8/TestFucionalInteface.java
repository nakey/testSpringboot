package com.xiechy.lamda8;

/**
 * @ClassName TestFucionalInteface
 * @Description 学习@FuctionnalInteface注解的用法
 * 这tmd不就是完美代理么？
 * 万能方法填充(这个很重要)
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2022/2/18 15:31
 */
public class TestFucionalInteface {

    public MyMethod myMethod;

    public TestFucionalInteface(MyMethod myMethod){
        this.myMethod = myMethod;
    }

    public void exec(){
        myMethod.excute();
    }

    @FunctionalInterface
    interface MyMethod{
        void excute() ;
    }
    public static void main(String[] args) {
        MyMethod type = ()-> System.out.println("函数式接口");
        type.excute();
        MyMethod type2 = TestFucionalInteface::say1;
        type2.excute();
        MyMethod type3 = TestFucionalInteface::say2;
        type3.excute();
        MyMethod type4 = TestJDK8::JDK8_stream;
        type4.excute();
        MyMethod type5 = null;
        printeType5(type5);

        TestFucionalInteface fucionalInteface = new TestFucionalInteface(Dog::test);
        fucionalInteface.exec();
    }

    private static void printeType5(MyMethod type5) {
        type5 =()->System.out.println("开始啦!");
        type5.excute();
        System.out.println("123");
    }

    public static void say1(){
        System.out.println("牛逼");
    }
    public  static void say2(){
        System.out.println("666");
    }
}
