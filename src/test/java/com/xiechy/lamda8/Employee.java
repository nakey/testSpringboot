package com.xiechy.lamda8;
import org.w3c.dom.NameList;
import java.util.Objects;
/**
* @author: wangwren
* @date: 2019/3/30
* @descripton:
* @version: 1.0
*/
public class Employee {
    private Integer id;
    private String name;
    private Integer age;
    private Double salary;
    private Status status;
    public Employee() {
    }
    public Employee(String name,Integer age){
        this.name = name;
        this.age = age;
    }
    public Employee(Integer id, String name, Integer age, Double salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    public Employee(Integer id, String name, Integer age, Double salary, Status status) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.status = status;
    }
    @Override
    public String toString() {
        return "Employee{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", age=" + age +
            ", salary=" + salary +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass())
        return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id) &&
            Objects.equals(name, employee.name) &&
            Objects.equals(age, employee.age) &&
            Objects.equals(salary, employee.salary);
    }

    @Override
    public int hashCode() {
    return Objects.hash(id, name, age, salary);
    }
    public enum Status{
        BUSY,
        FREE,
        VOCATION;
    }
    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }
    public String show(){
        return "测试方法引用";
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getAge() {
        return age;
    }
    public void setAge(Integer age) {
        this.age = age;
    }
    public Double getSalary() {
        return salary;
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }
}
