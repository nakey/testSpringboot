package com.xiechy.lamda8;

import org.junit.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @ClassName TestCompletableFuture
 * @Description 测试异步神器 CompletableFuture
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2023-01-30 15:38
 */
public class TestCompletableFuture {

    /**
     * 构造方法创建
     *
     * @throws ExecutionException   ExecutionException
     * @throws InterruptedException InterruptedException
     * @author nakey.xie
     * @date 2023/01/30
     */
    @Test
    public void helloWorld() throws ExecutionException, InterruptedException {
        CompletableFuture<String> completableFuture = new CompletableFuture<>();
        completableFuture.complete("hello completableFuture");
        System.out.println(completableFuture.get());
    }

    /**
     * 静态方法创建
     *
     * @author nakey.xie
     * @date 2023/01/30
     */
    @Test
    public void staticWay() throws ExecutionException, InterruptedException {
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(()-> "异步执行");
        System.out.println(completableFuture.get());

    }

}
