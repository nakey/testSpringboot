package com.xiechy.lamda8;


import org.junit.Test;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
public class Testlamda {


    /**
     * @Description 测试过滤器
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/11/2 18:09
     * @param
     * @return void
    */
    @Test
    public void testFilter(){
        List<Integer> num = Arrays.asList(new Integer[]{1,2,3,4,5,6});
        List<Integer> filterNum = num.stream().filter(i-> i!=5).collect(Collectors.toList());
        System.out.println(filterNum);//[1, 2, 3, 4, 6]
    }


    /**
     * 测试集合子循环对比
     *
     * @author nakey.xie
     * @date 2022/04/14
     */
    @Test
    public void testForEachSlef(){
        Map<Long, Set<String>> testMap = new HashMap<>();
        Set<String> set1 = new HashSet<>();
        set1.add("str1");
        set1.add("str2");
        set1.add("str3");
        testMap.put(1L, set1);
        Set<String> set2 = new HashSet<>();
        set2.add("str11");
        set2.add("str22");
        set2.add("str33");
        //故意设置重复元素str1
        set2.add("str1");
        testMap.put(2L, set2);
        //自身与自身对比
        testMap.forEach((k,v)->testMap.forEach((k1,v1)->{
            System.out.println("k:"+k);
            System.out.println("k1:"+k1);
            System.out.println("v:"+v);
            System.out.println("v1:"+v1);
            if(v.containsAll(v1)){
                System.out.println("列表一样");
            }
        }));
    }













    public static void main(String[] agrs) {
        List<Person> list = new ArrayList<Person>();
        list.add(new Person(1, "zhangsan"));
        list.add(new Person(2, "lisi"));
        Map testMap2 = getMap(list);
        System.out.println(testMap2.toString());
        System.out.println();
        System.out.println();
        System.out.println();
        try {

        } catch (Exception e) {

        }


    }

    private static Map getMap(List<Person> list) {
        Map testMap = list.stream().collect(Collectors.toMap(Person::getId, Person::getName));
        System.out.println(testMap.toString());
        return list.stream().collect(Collectors.toMap(Person::hashCode, Function.identity()));
    }


}

class Person {
    private int id;
    private String name;

    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
