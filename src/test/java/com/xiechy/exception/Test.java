package com.xiechy.exception;

public class Test {
    public static int test() {
         int a = 1;
        try {
            System.out.println(a / 0);
            a=2;
    }catch (ArithmeticException e) {
            a=3;
            return a;
        } finally {
            a=4;
        }
        return a;
    }

    public static void main(String[] args) {
        System.out.println(test());
    }
}


