package cn.llc.common.enums.activity;

/**
 * @EnumName ActivityBusinessType
 * @Description 触发活动业务类型
 * @Author xcy
 * @Date 2020/8/26 16:57
 */
public enum ActivityBusinessType {

    /**
     * 下单时触发
     */
    ORDER(1, "下单"),

    /**
     * 兑奖时触发同中同得
     */
    PRIZE(2, "兑奖"),

    /**
     * 绑定时触发新司机1分购
     */
    BINDING(3,"绑定"),

    /**
     * 定时任务每天凌晨触发一次阶梯奖励
     */
    STEP(4,"阶梯"),

    /**
     * 定时任务每天凌晨触发一次累计奖励
     */
    CUMULATIVE(5,"累计"),
    ;

    private int type;

    private String msg;

    ActivityBusinessType(int type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public int getType() {
        return this.type;
    }

    public String getMsg() {
        return this.msg;
    }

    public static String getMsg(int type) {
        String msg = "";
        for (ActivityBusinessType businessType : ActivityBusinessType.values()) {
            if (businessType.type == type) {
                msg = businessType.msg;
                return msg;
            }
        }
        return msg;
    }
}
