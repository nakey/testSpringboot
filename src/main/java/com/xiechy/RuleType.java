package com.xiechy;

/**
 * @EnumName RuleType
 * @Description 活动规则类型
 * @Author xcy
 * @Date 2020-9-16 14:31:01
 */
public enum RuleType {

    REWARD_AGENT("HDMB20200819001", "同中同得"),

    NEW_DRIVER("HDMB20201015001","新司机送券"),

    STEP_SALES_REWARD("HDMB20201015002","阶梯奖励"),

    CUMULATIVE_SALES_REWARD("HDMB20201015003","累计奖励"),
    ;
    //活动规则编号
    private String ruleNo;

    private String name;

    RuleType(String ruleNo, String name) {
        this.ruleNo = ruleNo;
        this.name = name;
    }

    public String getRule() {
        return this.ruleNo;
    }

    public String getName() {
        return this.name;
    }

    public static String getName(String ruleNo) {
        String name = "";
        for (RuleType businessType : RuleType.values()) {
            if (businessType.ruleNo == ruleNo) {
                name = businessType.name;
                return name;
            }
        }
        return name;
    }
}
