package com.xiechy.auto;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.highgui.HighGui;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.util.Objects;

public class OpencvUtils {
    private static final String BASE_PATH;

    static {
        BASE_PATH = OpencvUtils.class.getResource("/").getPath();
        String path = BASE_PATH + "opencv_java453.dll";
        System.load(path);
    }


    /**
     * 匹配
     *这个方法opencv做图像匹配，尺寸要一样
     * 有默认值
     * 返回模板图版在源图中的中心坐标
     * 你可以自已改代码返回你想要的
     * opencv没有maven
     * @param src      src
     * @param template 模板
     * @param methods  方法
     * @return {@link Point}
     * @author nakey.xie
     * @date 2022/02/23
     */
    public static Point match(Mat src, Mat template, int... methods) {
        int method = Imgproc.TM_CCOEFF_NORMED;
        if (Objects.nonNull(methods) && methods.length > 0) {
            method = methods[0];
        }
        int width = src.cols() - template.cols() + 1;
        int height = src.rows() - template.rows() + 1;
        Mat result = new Mat(width, height, CvType.CV_8UC4);
        Imgproc.matchTemplate(src, template, result, method);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Core.MinMaxLocResult locResult = Core.minMaxLoc(result);
        int x, y;
        if (method == Imgproc.TM_SQDIFF_NORMED || method == Imgproc.TM_SQDIFF) {
            x = (int) (locResult.minLoc.x + template.cols() / 2);
            y = (int) (locResult.minLoc.y + template.rows() / 2);
        } else {
            x = (int) (locResult.maxLoc.x + template.cols() / 2);
            y = (int) (locResult.maxLoc.y + template.rows() / 2);
        }

        return new Point(x, y);
    }

    public static Mat loadImg(String fileName) {
        return Imgcodecs.imread(fileName);
    }


    public static Mat getScreenShot() {
        //RobotOps robotOps=RobotOps.instance;
        BufferedImage bfImage = RobotOps.captureScreen();
        Mat mat =  new Mat(bfImage.getHeight(), bfImage.getWidth(), CvType.CV_8UC3);
        mat.put(0, 0,getMatrixRGB(bfImage));
        return mat;
    }

    /**
     * 获取图像RGB格式数据
     * @param image
     * @return
     */
    public static byte[] getMatrixRGB(BufferedImage image){
        if(image.getType()!=BufferedImage.TYPE_3BYTE_BGR){
            // 转sRGB格式
            BufferedImage rgbImage = new BufferedImage(
                    image.getWidth(),
                    image.getHeight(),
                    BufferedImage.TYPE_3BYTE_BGR);
            new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_sRGB), null).filter(image, rgbImage);
            // 从Raster对象中获取字节数组

            return  (byte[])((DataBufferByte)rgbImage.getRaster().getDataBuffer()).getData();
//            return (byte[]) rgbImage.getData().getDataElements(0, 0, rgbImage.getWidth(), rgbImage.getHeight(), null);
        }else{
            return (byte[]) image.getData().getDataElements(0, 0, image.getWidth(), image.getHeight(), null);
        }
    }

    public static void main(String[] args) {
        //RobotOps robotOps=RobotOps.instance;
        Mat mat = Imgcodecs.imread("D:\\git\\cloud-control\\ai-robot\\src\\main\\resources\\img\\login_username.png");
        Mat src = Imgcodecs.imread("D:\\git\\cloud-control\\ai-robot\\src\\main\\resources\\img\\src.png");
        HighGui.imshow("mat",mat);
        Point point = match(src, mat);
        RobotOps.mouseMove( point.x, point.y);
        System.out.println("point = " + point);
    }



}
