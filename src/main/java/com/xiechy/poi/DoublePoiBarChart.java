package com.xiechy.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * POI EXCEL 图表-柱状图(双柱状图)
 * ————————————————
 * 版权声明：本文为CSDN博主「小百菜」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/u014644574/article/details/105695787
 */
public class DoublePoiBarChart {
 
	public static void main(String[] args) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		String sheetName = "Sheet1";
		FileOutputStream fileOut = null;
		try {
			XSSFSheet sheet = wb.createSheet(sheetName);

			// 创建一个画布
			XSSFDrawing drawing = sheet.createDrawingPatriarch();
			// 14行，5列
			XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0,
					0, 5, 14);
			// 创建一个chart对象
			XSSFChart chart = drawing.createChart(anchor);
			// 标题
			chart.setTitleText("耗时分布分析");
			// 标题覆盖
			chart.setTitleOverlay(false);
 
			// 图例位置
			XDDFChartLegend legend = chart.getOrAddLegend();
			legend.setPosition(LegendPosition.TOP);
 
			// 分类轴标(X轴),标题位置
			XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
			bottomAxis.setTitle("类别");
			// 值(Y轴)轴,标题位置
			XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
			leftAxis.setTitle("时间");
 
			// CellRangeAddress(起始行号，终止行号， 起始列号，终止列号）
			// 分类轴标(X轴)数据，单元格范围位置[0, 0]到[0, 6]
			//XDDFDataSource<String> countries = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, 0, 0, 6));
			 XDDFCategoryDataSource category = XDDFDataSourcesFactory.fromArray(new String[] {"耗时"});
			// 数据1，单元格范围位置[1, 0]到[1, 6]
			//XDDFNumericalDataSource<Double> area = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(1, 1, 0, 6));
			 XDDFNumericalDataSource<Integer> data1 = XDDFDataSourcesFactory.fromArray(new Integer[] {700});
 
			// 数据2，单元格范围位置[2, 0]到[2, 6]
			//XDDFNumericalDataSource<Double> population = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(2, 2, 0, 6));
			XDDFNumericalDataSource<Integer> data2 = XDDFDataSourcesFactory.fromArray(new Integer[] {650});
			// bar：条形图，
			XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
 
			leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
			// 设置为可变颜色
			bar.setVaryColors(true);
			// 条形图方向，纵向/横向：纵向
			bar.setBarDirection(BarDirection.COL);
 
			// 图表加载数据，条形图1
			XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data1);
			// 条形图例标题
			series1.setTitle("应用按键响应耗时", null);
			XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
			// 条形图，填充颜色
			series1.setFillProperties(fill);
 
			// 图表加载数据，条形图2
			XDDFBarChartData.Series series2 = (XDDFBarChartData.Series) bar.addSeries(category, data2);
			// 条形图例标题
			series2.setTitle("player/mediacodec解码首帧耗时", null);
			XDDFSolidFillProperties fill2 = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.ORANGE));
			// 条形图，填充颜色
			series2.setFillProperties(fill2);
 
			// 绘制
			chart.plot(bar);
 
			// 打印图表的xml
			// System.out.println(chart.getCTChart());
 
			// 将输出写入excel文件
			String filename = "双柱图.xlsx";
			fileOut = new FileOutputStream(filename);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wb.close();
			if (fileOut != null) {
				fileOut.close();
			}
		}
 
	}
 
}

