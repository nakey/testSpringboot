package com.xiechy.poi;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.*;

public class MyBarChartTest {
	
	public static void main(String args[]) {
		String sheetName = "sheetname1";
		try (XSSFWorkbook wb = new XSSFWorkbook()){
			String  filePathDaily = "D:/云平台自动化测试开发/excel/";
			String fileName = "exceltest.xlsx";
			
			File filePath = new File(filePathDaily);
			if(!filePath.exists()&&!filePath.isDirectory()){
				filePath.mkdir();
			}
			//1.统计昨日交易量
			File file = new File(filePathDaily+fileName);
			
			List<String> title=Arrays.asList("序号","类型","业务","合计：交易量","合计：收入",
					"2019-01-01","2019-01-02","2019-01-03","2019-01-04","2019-01-05",
					"2019-01-01收入","2019-01-02收入","2019-01-03收入","2019-01-04收入","2019-01-05收入");
			List<String> titleStyle=Arrays.asList("int","String","String","int","double",
					"int","int","int","int","int",
					"double","double","double","double","double");
			List<String> dates = Arrays.asList("2019-01-01","2019-01-02","2019-01-03","2019-01-04","2019-01-05");
			
			Map<String, List<Object>> day2ColValueList=new LinkedHashMap<String, List<Object>>();
			day2ColValueList.put("类型1业务1",Arrays.asList("类型1","业务1",500,1000.00,100,50,100,150,100,200.00,150.00,200.00,250.00,200.00));
			day2ColValueList.put("类型1业务2",Arrays.asList("类型1","业务2",400,800.00,80,60,80,100,80,160.00,130.00,160.00,190.00,160.00));
			day2ColValueList.put("类型2业务1",Arrays.asList("类型2","业务1",600,1200.00,120,70,120,170,120,240.00,170.00,240.00,290.00,240.00));
			day2ColValueList.put("类型2业务2",Arrays.asList("类型2","业务2",200,400.00,40,140,40,80,40,80.00,100.00,80.00,60.00,80.00));
			MyExcleChart2.doWork(title, titleStyle, day2ColValueList,file,sheetName,wb,dates.size());


			XSSFWorkbook workbook = new XSSFWorkbook();
			XSSFSheet sheet = workbook.createSheet("Sheet1");
			XSSFSheet sheet2 = workbook.createSheet("Sheet2");

			List<int[]> params = new ArrayList<>();
			//坐标
			int [] coordinates = {0,0,7,16};
			params.add(coordinates);
			//设置测试次数
			Row row0 = sheet.createRow(18);
			row0.createCell(1).setCellValue(1);
			row0.createCell(2).setCellValue(2);
			row0.createCell(3).setCellValue(3);
			row0.createCell(4).setCellValue(4);
			row0.createCell(5).setCellValue(5);
			row0.createCell(6).setCellValue(6);

			//设置表格数据
			//4K零点
			Row row1 = sheet.createRow(19);
			row1.createCell(0).setCellValue("4k零点");
			row1.createCell(1).setCellValue(1200);
			row1.createCell(2).setCellValue(1300);
			row1.createCell(3).setCellValue(1400);
			row1.createCell(4).setCellValue(1115);
			row1.createCell(5).setCellValue(1250);
			row1.createCell(6).setCellValue(1350);
			//2k零点
			Row row2 = sheet.createRow(20);
			row2.createCell(0).setCellValue("2k零点");
			row2.createCell(1).setCellValue(1050);
			row2.createCell(2).setCellValue(950);
			row2.createCell(3).setCellValue(1200);
			row2.createCell(4).setCellValue(1300);
			row2.createCell(5).setCellValue(1100);
			row2.createCell(6).setCellValue(1000);
			//4k断点
			Row row3 = sheet.createRow(21);
			row3.createCell(0).setCellValue("4k断点");
			row3.createCell(1).setCellValue(1350);
			row3.createCell(2).setCellValue(1250);
			row3.createCell(3).setCellValue(1450);
			row3.createCell(4).setCellValue(1500);
			row3.createCell(5).setCellValue(1300);
			row3.createCell(6).setCellValue(1100);

			//2k断点
			Row row4 = sheet.createRow(22);
			row4.createCell(0).setCellValue("2k断点");
			row4.createCell(1).setCellValue(1200);
			row4.createCell(2).setCellValue(1150);
			row4.createCell(3).setCellValue(1110);
			row4.createCell(4).setCellValue(1000);
			row4.createCell(5).setCellValue(1100);
			row4.createCell(6).setCellValue(1200);

			//4k-seek
			Row row5 = sheet.createRow(23);
			row5.createCell(0).setCellValue("4k-seek");
			row5.createCell(1).setCellValue(500);
			row5.createCell(2).setCellValue(600);
			row5.createCell(3).setCellValue(400);
			row5.createCell(4).setCellValue(300);
			row5.createCell(5).setCellValue(550);
			row5.createCell(6).setCellValue(450);

			//2k-seek
			Row row6 = sheet.createRow(24);
			row6.createCell(0).setCellValue("2k-seek");
			row6.createCell(1).setCellValue(400);
			row6.createCell(2).setCellValue(350);
			row6.createCell(3).setCellValue(300);
			row6.createCell(4).setCellValue(250);
			row6.createCell(5).setCellValue(400);
			row6.createCell(6).setCellValue(500);

			//设置类别坐标
			int[] xAxis = {18,18,1,6};
			params.add(xAxis);
			//数据坐标;
			int [] data2 = {19,19,1,6,19,0};
			int [] data3 = {20,20,1,6,20,0};
			int [] data4 = {21,21,1,6,21,0};
			int [] data5 = {22,22,1,6,22,0};
			int [] data6 = {23,23,1,6,23,0};
			int [] data7 = {24,24,1,6,24,0};
			params.add(data2);
			params.add(data3);
			params.add(data4);
			params.add(data5);
			params.add(data6);
			params.add(data7);

			List<String> titles = Arrays.asList(new String[]{"腾讯vod起播速度各场景分布","测试次数","毫秒"});

			drawLineChart(sheet,sheet2,titles, params);

			//XSSFSheet sheet2 = workbook.createSheet("Sheet2");
			List<int[]> params2 = new ArrayList<>();
			//drawCTLineChart(sheet2, params2);
			FileOutputStream fileOut = new FileOutputStream("myExcel.xlsx");
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}



	/*
	 * @Description 画折线图
	 * @Author R&D-VAL SZ nakey.xie
	 * @Date  2021/12/22 11:15
	 * @param dataSheet 数据所在sheet
	 * @param drawSheet 需要画折线图的sheet(如果数据和画图在同一个sheet,可以传同一个)
	 * @param titles 折线图标题，x轴，y的标题，如果不设置标题，请设置空字符串，避免异常
	 * @param params params.get(0)为折线图表位置数组,下标0123分别为为起始横坐标，起始纵坐标，终点横，终点纵,注意终点横坐标其实是占几列，终点纵坐标其实是占几行
	 * @param params params.get(1) 为x轴取值范围，其他为折线取值范围。起始行号，终止行号， 起始列号，终止列号（折线名称坐标）。
	 * @param params params.get(2)  2以后都是数据源
	 * @return
	*/
	private static void drawLineChart(XSSFSheet dataSheet,XSSFSheet drawSheet,List<String> titles, List<int[]> params) {
		//折线图标题
		String chartTitle = "";
		//X轴标题
        String xTitle ="";
		//Y轴标题
        String yTitle ="";
		if(CollectionUtil.isNotEmpty(titles)){
			chartTitle = titles.get(0);
			xTitle = titles.get(1);
			yTitle = titles.get(2);
		}
		XSSFDrawing drawing = drawSheet.createDrawingPatriarch();
		// 设置位置 起始横坐标，起始纵坐标，终点横，终点纵
		XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, params.get(0)[0], params.get(0)[1], params.get(0)[2],
				params.get(0)[3]);
		XSSFChart chart = drawing.createChart(anchor);
		// 创建图形注释的位置
		XDDFChartLegend legend = chart.getOrAddLegend();
		legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.TOP_RIGHT);
		// 设置横坐标(底部X轴)
		XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.TOP);
		bottomAxis.setTitle(xTitle);

		// 设置纵坐标(左侧Y轴)
		XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
		leftAxis.setTitle(yTitle);
		leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

		// 创建绘图的类型 LineCahrtData 折线图
		XDDFLineChartData leftdata = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

		// 2 从Excel中获取数据.CellRangeAddress-->params: 起始行号，终止行号， 起始列号，终止列号

		// 2.1 数据类别（x轴）
		//从单元格获取数据类别
		CellRangeAddress cellRangeAddress = new CellRangeAddress(params.get(1)[0], params.get(1)[1], params.get(1)[2], params.get(1)[3]);
		XDDFDataSource<String> xAxis = XDDFDataSourcesFactory.fromStringCellRange(dataSheet, cellRangeAddress);
		// 2.2 获取每条折线的数据
		for (int i = 2, len = params.size(); i < len; i++) {
			// 数据源
			CellRangeAddress dataCellRangeAddress = new CellRangeAddress(params.get(i)[0], params.get(i)[1], params.get(i)[2], params.get(i)[3]);
			//纵轴为各个数据
			XDDFNumericalDataSource<Double> dataAxis = XDDFDataSourcesFactory.fromNumericCellRange(dataSheet, dataCellRangeAddress);

			XDDFLineChartData.Series chartSeries = (XDDFLineChartData.Series) leftdata.addSeries(xAxis, dataAxis);
			// 给每条折线创建名字
			chartSeries.setTitle(dataSheet.getRow(params.get(i)[4]).getCell(params.get(i)[5]).getStringCellValue(), null);
			//设置折线为直线
			chartSeries.setSmooth(false);
			// 设置标记样式 https://stackoverflow.com/questions/39636138
			chartSeries.setMarkerStyle(MarkerStyle.DASH);
		}
		setChartTitle(chart, chartTitle);
		// 开始绘制折线图
		chart.plot(leftdata);
	}

	private static void drawCTLineChart(XSSFSheet sheet, int[] position, List<String> xString, Set<String> serTxName,
										List<String> dataRef) {

		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		//Drawing drawing = sheet.createDrawingPatriarch();
		ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, position[0], position[1], position[2], position[3]);

		XSSFChart chart = drawing.createChart(anchor);

		CTChart ctChart = ((XSSFChart) chart).getCTChart();
		CTPlotArea ctPlotArea = ctChart.getPlotArea();
		CTLineChart ctLineChart = ctPlotArea.addNewLineChart();

		ctLineChart.addNewVaryColors().setVal(true);

		// telling the Chart that it has axis and giving them Ids
		setAxIds(ctLineChart);
		// set cat axis
		setCatAx(ctPlotArea);
		// set val axis
		setValAx(ctPlotArea);
		// legend
		setLegend(ctChart);
		// set data lable
		setDataLabel(ctLineChart);
		// set chart title
		setChartTitle((XSSFChart) chart, sheet.getSheetName());

		paddingData(ctLineChart, xString, serTxName, dataRef);
	}

	/*
	 * @param position 图表坐标 起始行，起始列，终点行，重点列
	 *
	 * @param xString 横坐标
	 *
	 * @param serTxName 图形示例
	 *
	 * @param dataRef 柱状图数据范围 ： sheetName!$A$1:$A$12
	 */
	private static void drawBarChart(XSSFSheet sheet, int[] position, List<String> xString, Set<String> serTxName,
									 List<String> dataRef) {

		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, position[0], position[1], position[2], position[3]);

		XSSFChart chart = drawing.createChart(anchor);

		CTChart ctChart = ((XSSFChart) chart).getCTChart();
		CTPlotArea ctPlotArea = ctChart.getPlotArea();
		CTBarChart ctBarChart = ctPlotArea.addNewBarChart();

		ctBarChart.addNewVaryColors().setVal(false);
		ctBarChart.addNewBarDir().setVal(STBarDir.COL);

		// telling the Chart that it has axis and giving them Ids
		setAxIds(ctBarChart);
		// set cat axis
		setCatAx(ctPlotArea);
		// set val axis
		setValAx(ctPlotArea);
		// add legend and set legend position
		setLegend(ctChart);
		// set data lable
		setDataLabel(ctBarChart);
		// set chart title
		setChartTitle((XSSFChart) chart, sheet.getSheetName());
		// padding data to chart
		paddingData(ctBarChart, xString, serTxName, dataRef);
	}

	private static void paddingData(CTBarChart ctBarChart, List<String> xString, Set<String> serTxName,
									List<String> dataRef) {
		Iterator<String> iterator = serTxName.iterator();
		for (int r = 0, len = dataRef.size(); r < len && iterator.hasNext(); r++) {
			CTBarSer ctBarSer = ctBarChart.addNewSer();

			ctBarSer.addNewIdx().setVal(r);
			// set legend value
			setLegend(iterator.next(), ctBarSer.addNewTx());
			// cat ax value
			setChartCatAxLabel(ctBarSer.addNewCat(), xString);
			// value range
			ctBarSer.addNewVal().addNewNumRef().setF(dataRef.get(r));
			// add border to chart
			ctBarSer.addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[] { 0, 0, 0 });
		}
	}

	private static void setLegend(String str, CTSerTx ctSerTx) {
		if (str.contains("$"))
			// set legend by str ref
			ctSerTx.addNewStrRef().setF(str);
		else
			// set legend by str
			ctSerTx.setV(str);
	}

	private static void paddingData(CTLineChart ctLineChart, List<String> xString, Set<String> serTxName,
									List<String> dataRef) {
		Iterator<String> iterator = serTxName.iterator();
		for (int r = 0, len = dataRef.size(); r < len && iterator.hasNext(); r++) {
			CTLineSer ctLineSer = ctLineChart.addNewSer();
			ctLineSer.addNewIdx().setVal(r);
			setLegend(iterator.next(), ctLineSer.addNewTx());
			setChartCatAxLabel(ctLineSer.addNewCat(), xString);
			ctLineSer.addNewVal().addNewNumRef().setF(dataRef.get(r));
			ctLineSer.addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[] { 0, 0, 0 });
		}
	}

	private static void setChartCatAxLabel(CTAxDataSource cttAxDataSource, List<String> xString) {
		if (xString.size() == 1) {
			cttAxDataSource.addNewStrRef().setF(xString.get(0));
		} else {
			CTStrData ctStrData = cttAxDataSource.addNewStrLit();
			for (int m = 0, xlen = xString.size(); m < xlen; m++) {
				CTStrVal ctStrVal = ctStrData.addNewPt();
				ctStrVal.setIdx((long) m);
				ctStrVal.setV(xString.get(m));
			}
		}
	}

	private static void setDataLabel(CTBarChart ctBarChart) {
		setDLShowOpts(ctBarChart.addNewDLbls());
	}

	private static void setDataLabel(CTLineChart ctLineChart) {
		CTDLbls dlbls = ctLineChart.addNewDLbls();
		setDLShowOpts(dlbls);
		//setDLPosition(dlbls, null);
	}

	/*private static void setDLPosition(CTDLbls dlbls, STDLblPos.Enum e) {
		if (e == null)
			dlbls.addNewDLblPos().setVal(STDLblPos.T);
		else
			dlbls.addNewDLblPos().setVal(e);
	}*/

	private static void setDLShowOpts(CTDLbls dlbls) {
		// 添加图形示例的字符串
		dlbls.addNewShowSerName().setVal(false);
		// 添加x轴的坐标字符串
		dlbls.addNewShowCatName().setVal(false);
		// 添加图形示例的图片
		dlbls.addNewShowLegendKey().setVal(false);
		// 添加x对应y的值---全设置成false 就没什么用处了
		// dlbls.addNewShowVal().setVal(false);
	}

	private static void setAxIds(CTBarChart ctBarChart) {
		ctBarChart.addNewAxId().setVal(123456);
		ctBarChart.addNewAxId().setVal(123457);
	}

	private static void setAxIds(CTLineChart ctLineChart) {
		ctLineChart.addNewAxId().setVal(123456);
		ctLineChart.addNewAxId().setVal(123457);
	}

	private static void setLegend(CTChart ctChart) {
		CTLegend ctLegend = ctChart.addNewLegend();
		ctLegend.addNewLegendPos().setVal(STLegendPos.B);
		ctLegend.addNewOverlay().setVal(false);
	}

	private static void setCatAx(CTPlotArea ctPlotArea) {
		CTCatAx ctCatAx = ctPlotArea.addNewCatAx();
		ctCatAx.addNewAxId().setVal(123456); // id of the cat axis
		CTScaling ctScaling = ctCatAx.addNewScaling();
		ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
		ctCatAx.addNewDelete().setVal(false);
		ctCatAx.addNewAxPos().setVal(STAxPos.B);
		ctCatAx.addNewCrossAx().setVal(123457); // id of the val axis
		ctCatAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);

	}

	// 不要y轴的标签，或者y轴尽可能的窄一些
	private static void setValAx(CTPlotArea ctPlotArea) {
		CTValAx ctValAx = ctPlotArea.addNewValAx();
		ctValAx.addNewAxId().setVal(123457); // id of the val axis
		CTScaling ctScaling = ctValAx.addNewScaling();
		ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
		// 不现实y轴
		ctValAx.addNewDelete().setVal(true);
		ctValAx.addNewAxPos().setVal(STAxPos.L);
		ctValAx.addNewCrossAx().setVal(123456); // id of the cat axis
		ctValAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);
	}

	// 图标标题
	private static void setChartTitle(XSSFChart xchart, String titleStr) {
		CTChart ctChart = xchart.getCTChart();

		CTTitle title = CTTitle.Factory.newInstance();
		CTTx cttx = title.addNewTx();
		CTStrData sd = CTStrData.Factory.newInstance();
		CTStrVal str = sd.addNewPt();
		str.setIdx(123459);
		str.setV(titleStr);
		cttx.addNewStrRef().setStrCache(sd);

		ctChart.setTitle(title);
	}

}



