package com.xiechy.poi;

import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * POI EXCEL 图表-柱状图(双柱状图)
 * ————————————————
 * 版权声明：本文为CSDN博主「小百菜」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/u014644574/article/details/105695787
 */
public class MyDoubleBarChart {

	public static void main(String[] args) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		String sheetName = "Sheet1";
		FileOutputStream fileOut = null;
		try {
			XSSFSheet sheet = wb.createSheet(sheetName);
			// 15行，7列
			List<Integer> rowCols = Arrays.asList(new Integer[]{0,0,7,15});
			List<String> titles = Arrays.asList(new String[]{"不同应用明显卡顿次数对比","应用","次数","缓冲次数","卡顿次数"});
			String[] categoryValue ={"斗鱼", "虎牙", "腾讯vod", "奇异果", "小电视"};
			Integer[] firstDataValue={5, 2, 1, 3, 3};
			Integer[] secDataValue={3, 5, 2, 3, 8};

			drawDoubleBarChart(sheet, rowCols, titles, categoryValue, firstDataValue, secDataValue);
			// 打印图表的xml
			// System.out.println(chart.getCTChart());
			// 将输出写入excel文件
			String filename = "不同应用明显卡顿次数对比.xlsx";
			fileOut = new FileOutputStream(filename);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wb.close();
			if (fileOut != null) {
				fileOut.close();
			}
		}

	}


	/**
	 * @Description 画双柱状图
	 * @Author R&D-VAL SZ nakey.xie
	 * @Date  2021/12/22 17:05
	 * @param sheet excel页签
	 * @param rowCols 画图位置:起始列号、起始行号、终止列号、终止行号
	 * @param titles 标题:总标题、X轴标题、Y轴标题、条形图1标题、条形图2标题
	 * @param categoryValue 类别数据
	 * @param firstDataValue 条形图1具体数据
	 * @param secDataValue 条形图2具体数据
	 * @return void 
	*/
	public static void drawDoubleBarChart(XSSFSheet sheet, List<Integer> rowCols, List<String> titles,
										  String[] categoryValue, Integer[] firstDataValue, Integer[] secDataValue) {

		// 创建一个画布
		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		// 设置位置
		XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
				rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
		// 创建一个chart对象
		XSSFChart chart = drawing.createChart(anchor);
		// 标题
		chart.setTitleText(titles.get(0));
		// 标题覆盖
		chart.setTitleOverlay(false);
		// 图例位置
		XDDFChartLegend legend = chart.getOrAddLegend();
		legend.setPosition(LegendPosition.TOP);
		// 分类轴标(X轴),标题位置
		XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
		bottomAxis.setTitle(titles.get(1));
		// 值(Y轴)轴,标题位置
		XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
		leftAxis.setTitle(titles.get(2));

		XDDFCategoryDataSource category = XDDFDataSourcesFactory.fromArray(categoryValue);
		//条形图1具体数据
		XDDFNumericalDataSource<Integer> data1 = XDDFDataSourcesFactory.fromArray(firstDataValue);
		//条形图2具体数据
		XDDFNumericalDataSource<Integer> data2 = XDDFDataSourcesFactory.fromArray(secDataValue);
		// bar：条形图，
		XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);

		leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
		// 设置为可变颜色
		bar.setVaryColors(true);
		// 条形图方向，纵向/横向：纵向
		bar.setBarDirection(BarDirection.COL);

		// 图表加载数据，条形图1
		XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data1);
		// 条形图例标题
		series1.setTitle(titles.get(3), null);
		XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
		// 条形图，填充颜色
		series1.setFillProperties(fill);

		// 图表加载数据，条形图2
		XDDFBarChartData.Series series2 = (XDDFBarChartData.Series) bar.addSeries(category, data2);
		// 条形图例标题
		series2.setTitle(titles.get(4), null);
		XDDFSolidFillProperties fill2 = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.ORANGE));
		// 条形图，填充颜色
		series2.setFillProperties(fill2);
		// 绘制
		chart.plot(bar);
	}
 
}

