package com.xiechy.poi;

import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import org.eclipse.jdt.internal.compiler.classfmt.MethodInfoWithAnnotations;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @ClassName PieChart
 * @Description 饼状图
 * @Author R&D-VAL SZ nakey.xie
 * @Date 2021/12/22 16:20
 */
public class PieChart {

    public static void main(String[] args) throws IOException {
        String title = "用例结果";
        String[] categoryValue ={"用例成功数","用例失败数","用例异常数"};
        Integer[] dataValue={4,1,1};
        XSSFWorkbook wb = new XSSFWorkbook();
        String sheetName = "Sheet1";
        FileOutputStream fileOut = null;
        //12行4列
        List<Integer> rowCal =Arrays.asList(new Integer[]{0,3,4,15});
        try {
            XSSFSheet sheet = wb.createSheet(sheetName);

            drawPieChart(sheet, rowCal, title, categoryValue, dataValue);
            // 将输出写入excel文件
            String filename = "饼图测试.xlsx";
            fileOut = new FileOutputStream(filename);
            wb.write(fileOut);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            wb.close();
            if (fileOut != null) {
                fileOut.close();
            }
        }
    }


    /**
     * @Description 画饼图
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 16:35
     * @param sheet excel页签
     * @param rowCols 画图位置:起始列号、起始行号、终止列号、终止行号
     * @param title 标题
     * @param categoryValue 类别数据
     * @param dataValue 具体数据
     * @return void
    */
    public static void drawPieChart(XSSFSheet sheet, List<Integer> rowCols, String title,
                                    String[] categoryValue, Integer[] dataValue) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
                rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
        XSSFChart chart = drawing.createChart(anchor);
        chart.setTitleText(title);
        chart.setTitleOverlay(false);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);

        XDDFDataSource<String> category = XDDFDataSourcesFactory.fromArray(categoryValue);
        // 数据1，单元格范围位置
        //XDDFNumericalDataSource<Double> data = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, 1, 1, 1));
        XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataValue);

        XDDFChartData chartData = chart.createData(ChartTypes.PIE, null, null);
        chartData.setVaryColors(true);
        chartData.addSeries(category, data);
        chart.plot(chartData);
    }
}
