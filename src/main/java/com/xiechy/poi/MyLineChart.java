package com.xiechy.poi;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @Description 折线图
 * @Author R&D-VAL SZ nakey.xie
 * @Date  2021/12/22 17:19
 * @param
 * @return
*/
public class MyLineChart {

	public static void main(String args[]) throws IOException {

		List<String> titles = Arrays.asList(new String[]{"腾讯vod起播速度各场景分布","测试次数","毫秒"});
		List<String> dataTitles = Arrays.asList(new String[]{"4k零点","2k零点","4k断点","2k断点","4k-seek","2k-seek"});
		String[] categoryValue ={"1","2","3","4","5","6"};
		List<Integer[]> dataValue=new ArrayList<>();
		Integer[] data1 ={1200,1300,1400,1115,1250,1350};
		Integer[] data2 ={1050,950,1200,1300,1100,1000};
		Integer[] data3 ={1350,1250,1450,1500,1300,1100};
		Integer[] data4 ={1200,1150,1110,1000,1100,1200};
		Integer[] data5 ={500,600,400,300,550,450};
		Integer[] data6 ={400,350,300,250,400,500};
		dataValue.add(data1);
		dataValue.add(data2);
		dataValue.add(data3);
		dataValue.add(data4);
		dataValue.add(data5);
		dataValue.add(data6);
		XSSFWorkbook wb = new XSSFWorkbook();
		String sheetName = "Sheet1";
		FileOutputStream fileOut = null;
		//16行，7列
		List<Integer> rowCols =Arrays.asList(new Integer[]{0,0,7,16});
		try {
			XSSFSheet sheet = wb.createSheet(sheetName);

			drawLineChart(sheet,titles,rowCols, categoryValue, dataValue, dataTitles);
			// 将输出写入excel文件
			String filename = "折线图输出.xlsx";
			fileOut = new FileOutputStream(filename);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wb.close();
			if (fileOut != null) {
				fileOut.close();
			}
		}


	}



	/*
	 * @Description 画折线图
	 * @Author R&D-VAL SZ nakey.xie
	 * @Date  2021/12/22 11:15
	 * @param sheet 数据所在sheet
	 * @param titles 折线图标题，x轴，y的标题，如果不设置标题，请设置空字符串，避免异常
	 * @param rowCols 设置画图位置:起始列号、起始行号、终止列号、终止行号
	 * @param categoryValue 类别
	 * @param dataValue 具体数据
	 * @param dataTitles 折线标题
	 * @return
	*/
	private static void drawLineChart(XSSFSheet sheet,List<String> titles, List<Integer> rowCols, String[] categoryValue,
									  List<Integer[]> dataValue, List<String> dataTitles) {

		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		// 设置位置 起始横坐标，起始纵坐标，终点横，终点纵
		XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
				rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
		XSSFChart chart = drawing.createChart(anchor);
		// 创建图形注释的位置
		XDDFChartLegend legend = chart.getOrAddLegend();
		legend.setPosition(LegendPosition.TOP_RIGHT);
		// 设置横坐标(底部X轴)
		XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.TOP);
		bottomAxis.setTitle(titles.get(1));

		// 设置纵坐标(左侧Y轴)
		XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
		leftAxis.setTitle(titles.get(2));
		leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

		// 创建绘图的类型 LineCahrtData 折线图
		XDDFLineChartData leftdata = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

		// 2 设置数据
		// 2.1 数据类别（x轴）
		XDDFDataSource<String> category = XDDFDataSourcesFactory.fromArray(categoryValue);
		// 2.2 获取每条折线的数据
		for (int i = 0; i < dataValue.size();  i++) {
			Integer[] dataArr = dataValue.get(i);

			XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataArr);

			XDDFLineChartData.Series chartSeries = (XDDFLineChartData.Series) leftdata.addSeries(category, data);
			// 给每条折线创建名字
			chartSeries.setTitle(dataTitles.get(i), null);
			//设置折线为直线
			chartSeries.setSmooth(false);
			// 设置标记样式 https://stackoverflow.com/questions/39636138
			chartSeries.setMarkerStyle(MarkerStyle.DASH);
		}
		setChartTitle(chart, titles.get(0));
		// 开始绘制折线图
		chart.plot(leftdata);
	}



	// 图标标题
	private static void setChartTitle(XSSFChart xchart, String titleStr) {
		CTChart ctChart = xchart.getCTChart();

		CTTitle title = CTTitle.Factory.newInstance();
		CTTx cttx = title.addNewTx();
		CTStrData sd = CTStrData.Factory.newInstance();
		CTStrVal str = sd.addNewPt();
		str.setIdx(123459);
		str.setV(titleStr);
		cttx.addNewStrRef().setStrCache(sd);

		ctChart.setTitle(title);
	}

}



