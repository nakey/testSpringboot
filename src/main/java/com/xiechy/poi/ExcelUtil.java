package com.xiechy.poi;

import cn.hutool.core.collection.CollectionUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wangpan
 * @version 1.0
 * 2021/3/3 11:15 上午
 */
public class ExcelUtil {

    public static final Map<String, String> PERFORMANCE_CASE_NAME = new HashMap<>();

    static {
        PERFORMANCE_CASE_NAME.put("冷启动", "coldStart");
        PERFORMANCE_CASE_NAME.put("热启动", "hotStart");
        PERFORMANCE_CASE_NAME.put("帧率", "frame");
        PERFORMANCE_CASE_NAME.put("常驻内存", "persist");
        PERFORMANCE_CASE_NAME.put("空载内存", "idleMemory");
        PERFORMANCE_CASE_NAME.put("压力内存", "pressureMemory");
        PERFORMANCE_CASE_NAME.put("遍历内存", "recursiveMemory");
        PERFORMANCE_CASE_NAME.put("后台内存", "backgroundMemory");
        PERFORMANCE_CASE_NAME.put("空载CPU", "idleCpu");
        PERFORMANCE_CASE_NAME.put("压力CPU", "pressureCpu");
        PERFORMANCE_CASE_NAME.put("遍历CPU", "recursiveCpu");
        PERFORMANCE_CASE_NAME.put("后台CPU", "backgroundCpu");
        PERFORMANCE_CASE_NAME.put("空载IOW", "idleIow");
        PERFORMANCE_CASE_NAME.put("压力IOW", "pressureIow");
        PERFORMANCE_CASE_NAME.put("遍历IOW", "recursiveIow");
        PERFORMANCE_CASE_NAME.put("后台IOW", "backgroundIow");
        PERFORMANCE_CASE_NAME.put("空载flash", "idleFlash");
        PERFORMANCE_CASE_NAME.put("压力flash", "pressureFlash");
        PERFORMANCE_CASE_NAME.put("遍历fLash", "recursiveFlash");
        PERFORMANCE_CASE_NAME.put("后台flash", "backgroundFlash");
        PERFORMANCE_CASE_NAME.put("开机自启", "power");
        PERFORMANCE_CASE_NAME.put("安装占用", "spaceOccupied");
    }

    public static final String[] PERFORMANCE_TEST_SUMMARY = {"用例总数", "", "用例通过率", "", "通过", "", "未通过", "", "未测试", ""};
    public static final String[] PERFORMANCE_TEST_MORE_INFO = {"测试人员", "", "测试时间", "", "测试环境", ""};
    public static final String[] PERFORMANCE_TEST_APP_DESC = {"APK大小", "", "应用来源", "", "应用类别", ""};
    public static final String[] PERFORMANCE_TEST_APP_TITLE = {"应用名", "", "包名", "", "应用版本", "", "文件Md5", ""};
    public static final String[] PERFORMANCE_TEST_TV_TITLE = {"测试机型", "", "系统版本", ""};
    public static final String[] PERFORMANCE_TEST_CASE_TITLE = {"用例名", "均值", "最大值", "最小值", "方差"};
    public static final String PERFORMANCE_TEST_DESC = "1.测试应用和测试设备必填\r\n" +
            "2.常驻内存和开机自启只需填写均值项即可（0-不自启, 1-自启）\r\n" +
            "3.未测试用例无需填写数据,非常驻内存和开机自启测试项测试数据均需填写";

    public static final String[] EXCEPTION_SUMMARY_TITLE = {"序号", "机型", "设备序列号", "异常应用", "异常类型", "异常次数", "统计时间"};

    public static final String[] PERFORMANCE_TEST_RESULT_SUMMARY_TITLE = {"项目名称", "机型", "系统版本", "应用版本", "测试人员", "测试日期"};

    public static final String[] STABLE_TASK_HEADER = {"任务执行总数据"};
    public static final String[] STABLE_TEST_EXCEL_TV_TITLE = {"设备名", "设备序列号", "机型", "系统版本", "硬件型号", "机芯", "MAC地址", "ANDROID版本", "DUM"};
    public static final String[] EXCEPTION_TITLE = {"异常类型", "发生时间", "包名", "日志地址"};

    public static final String[] MONKEY_TASK_TITLE = {"任务名称", "机器数量", "执行总时长", "成功总次数", "失败总次数", "异常总数", "ANR总数", "CRASH总数"};
    public static final String[] TV_EXECUTE_HEAD = {"各测试机器数据"};
    public static final String[] MONKEY_TV_EXECUTE_TITLE = {"序号", "机器名称", "执行时间", "执行成功次数", "执行失败次数", "异常次数", "ANR次数", "CRASH次数", "设备状态"};
    public static final String[] COMMON_TEST_TITLE = {"用例编号", "用例描述", "前置条件", "操作步骤", "测试结果"};


    public static final String[] RECURSIVE_HEADER = {"功能遍历执行"};
    public static final String[] RECURSIVE_TASK_TITLE = {"任务名称", "机器数量", "执行总时长", "执行次数", "异常总数", "ANR总数", "CRASH总数", "CPU告警次数", "MEMORY告警次数"};
    public static final String[] RECURSIVE_TASK_EXECUTE_TITLE = {"脚本包名", "测试时间", "执行次数", "异常次数"};
    public static final String[] RECURSIVE_TV_EXECUTE_TITLE = {"序号", "机器名称", "测试时间", "执行次数", "异常次数", "ANR次数", "CRASH次数", "CPU告警次数", "MEMORY告警次数", "设备状态"};

    public static final String[] FIXED_SCENE_HEADER = {"脚本执行数据"};
    public static final String[] FIXED_SCENE_TITLE = {"任务名称", "机器数量", "执行总时长", "执行次数", "异常总数", "ANR总数", "CRASH总数", "CPU告警次数", "MEMORY告警次数"};
    public static final String[] FIXED_SCENE_EXECUTE_TITLE = {"脚本名称", "脚本执行次数", "执行失败次数", "执行时间"};
    public static final String[] FIXED_SCENE_TV_EXECUTE_TITLE = {"序号", "机器名称", "测试时间", "执行次数", "执行失败测试次数", "异常次数", "ANR次数", "CRASH次数", "CPU告警次数", "MEMORY告警次数", "设备状态"};


    public static final String[] POWER_TITLE = {"任务名称", "机器数量", "执行总时长", "执行次数", "开关机成功次数", "WIFI重连失败次数", "蓝牙启动失败次数", "界面异常次数", "异常总次数", "CRASH总数", "ANR次数"};
    public static final String[] POWER_TV_EXECUTE_TITLE = {"序号", "机器名称", "执行时间", "执行次数", "开关机成功次数", "WIFI重连失败次数", "蓝牙启动失败次数", "界面异常次数", "异常次数", "CRASH次数", "ANR次数", "设备状态"};


    public static final String[] STABLE_EXCEPTION_SUMMARY_HEADER = {"测试异常汇总"};
    public static final String[] TEST_RESULT_SUMMARY_HEADER = {"脚本执行失败点汇总"};

    public static final String[] STABLE_EXCEPTION_SUMMARY_TITLE = {"机器名称", "异常类型", "包名", "发生时间", "日志地址"};
    public static final String[] STABLE_FAIL_TEST_RESULT_SUMMARY_TITLE = {"机器名称", "发生时间", "结束时间", "失败描述"};


    public static final String[] PROJECTION_TASK_SUMMARY_TITLE = {"任务名", "运行时间", "手机型号", "手机系统版本", "TV别名", "TV型号", "TV系统版本", "用例成功数", "用例失败数", "用例异常数", "用例总数", "得分"};
    public static final String[] PROJECTION_CASE_SUMMARY_TITLE = {"脚本名", "用例名", "运行时间", "成功次数", "失败次数", "异常次数", "总次数", "用例得分"};
    public static final String[] PROJECTION_CARE_RESULT_TITLE = {"脚本名", "用例名", "开始时间", "结束时间", "日志路径", "执行结果"};

    public static final String[] USER_FEEDBACK_TITLE = {"标题", "详情", "附件地址", "用户", "时间"};

    public static final String[] MEDIA_SUMMARY_TITLE = {"多媒体播放流畅性测试报告"};
    public static final String[] FIRST_LEVEL_SUMMARY_TITLE = {"汇总数据"};
    public static final String[] FRAME_TEST_SUMMARY_TITLE = {"模块", "测试场景", "用例执行数（片源数）", "启播平均值", "得分", "综合得分"};
    public static final String[] PLAY_TEST_SUMMARY_TITLE = {"模块", "用例执行数（片源数）", "成功率", "失败率", "综合得分"};
    public static final String[] LAG_TEST_SUMMARY_TITLE = {"模块", "用例执行数（片源数）", "测试时长", "卡顿时长", "综合得分"};
    public static final String[] AV_TEST_SUMMARY_TITLE = {"模块", "用例执行数（片源数）", "测试时长", "异常数", "异常率", "综合得分"};

    public static final String[] FRAME_TEST_DETAIL_TITLE = {"首帧速度", "用例名称", "脚本名称", "首帧时间", "得分", "关键日志"};
    public static final String[] PLAY_TEST_DETAIL_TITLE = {"开播检测", "用例名称", "脚本名称", "测试结果", "关键日志"};
    public static final String[] LAG_TEST_DETAIL_TITLE = {"百秒卡顿", "用例名称", "脚本名称", "影片播放时长", "卡顿次数", "卡顿时长", "关键日志"};
    public static final String[] AV_TEST_DETAIL_TITLE = {"音画异常", "用例名称", "脚本名称", "影片播放时长", "异常数", "关键日志"};

    public final static SimpleDateFormat COMMON_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    public static void writeSummaryTitle(Workbook workbook, Sheet sheet, String[] summaryTitle, int titleIndex) {
        writeSummaryTitle(workbook, sheet, summaryTitle, titleIndex, true);
    }

    public static void writeSummaryTitle(Workbook workbook, Sheet sheet, String[] summaryTitle, int titleIndex, boolean writeColor) {
        if (writeColor) {
            CellStyle titleStyle = workbook.createCellStyle();
            titleStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
            titleStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            Row title = sheet.createRow(titleIndex);
            sheet.setColumnWidth(1, 20 * 256);
            AtomicInteger index = new AtomicInteger();
            Arrays.stream(summaryTitle).forEach(columnName -> {
                Cell cell = title.createCell(index.get());
                cell.setCellValue(columnName);
                cell.setCellStyle(titleStyle);
                index.getAndIncrement();
            });
        } else {
            Row title = sheet.createRow(titleIndex);
            sheet.setColumnWidth(1, 20 * 256);
            AtomicInteger index = new AtomicInteger();
            Arrays.stream(summaryTitle).forEach(columnName -> {
                Cell cell = title.createCell(index.get());
                cell.setCellValue(columnName);
                index.getAndIncrement();
            });
        }
    }


    /*
     * @Description 画折线图(从表格获取值)
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 11:15
     * @param dataSheet 数据所在sheet
     * @param drawSheet 需要画折线图的sheet(如果数据和画图在同一个sheet,可以传同一个)
     * @param titles 折线图标题，x轴，y的标题，如果不设置标题，请设置空字符串，避免异常
     * @param params params.get(0)为折线图表位置数组,下标0123分别为为起始横坐标，起始纵坐标，终点横，终点纵,注意终点横坐标其实是占几列，终点纵坐标其实是占几行
     * @param params params.get(1) 为x轴取值范围，其他为折线取值范围。起始行号，终止行号， 起始列号，终止列号（折线名称坐标）。
     * @param params params.get(2)  2以后都是数据源
     * @return
     */
    public static void drawLineChart(XSSFSheet dataSheet, XSSFSheet drawSheet, List<String> titles, List<int[]> params) {
        //折线图标题
        String chartTitle = "";
        //X轴标题
        String xTitle ="";
        //Y轴标题
        String yTitle ="";
        if(CollectionUtil.isNotEmpty(titles)){
            chartTitle = titles.get(0);
            xTitle = titles.get(1);
            yTitle = titles.get(2);
        }
        XSSFDrawing drawing = drawSheet.createDrawingPatriarch();
        // 设置位置 起始横坐标，起始纵坐标，终点横，终点纵
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, params.get(0)[0], params.get(0)[1], params.get(0)[2],
                params.get(0)[3]);
        XSSFChart chart = drawing.createChart(anchor);
        // 创建图形注释的位置
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);
        // 设置横坐标(底部X轴)
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.TOP);
        bottomAxis.setTitle(xTitle);

        // 设置纵坐标(左侧Y轴)
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle(yTitle);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

        // 创建绘图的类型 LineCahrtData 折线图
        XDDFLineChartData leftdata = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

        // 2 从Excel中获取数据.CellRangeAddress-->params: 起始行号，终止行号， 起始列号，终止列号

        // 2.1 数据类别（x轴）
        //从单元格获取数据类别
        CellRangeAddress cellRangeAddress = new CellRangeAddress(params.get(1)[0], params.get(1)[1], params.get(1)[2], params.get(1)[3]);
        XDDFDataSource<String> xAxis = XDDFDataSourcesFactory.fromStringCellRange(dataSheet, cellRangeAddress);
        // 2.2 获取每条折线的数据
        for (int i = 2, len = params.size(); i < len; i++) {
            // 数据源
            CellRangeAddress dataCellRangeAddress = new CellRangeAddress(params.get(i)[0], params.get(i)[1], params.get(i)[2], params.get(i)[3]);
            //纵轴为各个数据
            XDDFNumericalDataSource<Double> dataAxis = XDDFDataSourcesFactory.fromNumericCellRange(dataSheet, dataCellRangeAddress);

            XDDFLineChartData.Series chartSeries = (XDDFLineChartData.Series) leftdata.addSeries(xAxis, dataAxis);
            // 给每条折线创建名字
            chartSeries.setTitle(dataSheet.getRow(params.get(i)[4]).getCell(params.get(i)[5]).getStringCellValue(), null);
            //设置折线为直线
            chartSeries.setSmooth(false);
            // 设置标记样式https://stackoverflow.com/questions/39636138
            chartSeries.setMarkerStyle(MarkerStyle.DASH);
        }
        setChartTitle(chart, chartTitle);
        // 开始绘制折线图
        chart.plot(leftdata);
    }


    /*
     * @Description 画折线图(直接传值)
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 11:15
     * @param sheet 数据所在sheet
     * @param titles 折线图标题，x轴，y的标题，如果不设置标题，请设置空字符串，避免异常
     * @param rowCols 设置画图位置:起始列号、起始行号、终止列号、终止行号
     * @param categoryValue 类别
     * @param dataValue 具体数据
     * @param dataTitles 折线标题
     * @return
     */
    private static void drawLineChart(XSSFSheet sheet,List<String> titles, List<Integer> rowCols, String[] categoryValue,
                                      List<Integer[]> dataValue, List<String> dataTitles) {

        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        // 设置位置 起始横坐标，起始纵坐标，终点横，终点纵
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
                rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
        XSSFChart chart = drawing.createChart(anchor);
        // 创建图形注释的位置
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);
        // 设置横坐标(底部X轴)
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.TOP);
        bottomAxis.setTitle(titles.get(1));

        // 设置纵坐标(左侧Y轴)
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle(titles.get(2));
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);

        // 创建绘图的类型 LineCahrtData 折线图
        XDDFLineChartData leftdata = (XDDFLineChartData) chart.createData(ChartTypes.LINE, bottomAxis, leftAxis);

        // 2 设置数据
        // 2.1 数据类别（x轴）
        XDDFDataSource<String> category = XDDFDataSourcesFactory.fromArray(categoryValue);
        // 2.2 获取每条折线的数据
        for (int i = 0; i < dataValue.size();  i++) {
            Integer[] dataArr = dataValue.get(i);

            XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataArr);

            XDDFLineChartData.Series chartSeries = (XDDFLineChartData.Series) leftdata.addSeries(category, data);
            // 给每条折线创建名字
            chartSeries.setTitle(dataTitles.get(i), null);
            //设置折线为直线
            chartSeries.setSmooth(false);
            // 设置标记样式 https://stackoverflow.com/questions/39636138
            chartSeries.setMarkerStyle(MarkerStyle.DASH);
        }
        setChartTitle(chart, titles.get(0));
        // 开始绘制折线图
        chart.plot(leftdata);
    }

    /**
     * @Description 设置图标标题
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 11:47
     * @param xchart
     * @param titleStr
     * @return void
    */
    private static void setChartTitle(XSSFChart xchart, String titleStr) {
        CTChart ctChart = xchart.getCTChart();
        CTTitle title = CTTitle.Factory.newInstance();
        CTTx cttx = title.addNewTx();
        CTStrData sd = CTStrData.Factory.newInstance();
        CTStrVal str = sd.addNewPt();
        str.setIdx(123459);
        str.setV(titleStr);
        cttx.addNewStrRef().setStrCache(sd);
        ctChart.setTitle(title);
    }


    /**
     * @Description 画单柱状图（可横可竖）
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 15:03
     * @param sheet excel页
     * @param direction col为竖向,bar为横向
     * @param titles 总标题，x轴标题,轴标题，不设置填写空串
     * @param categoryValue x轴类别
     * @param dataValue 具体数值
     * @param rowCols 柱状图位置:起始列号、起始行号、终止列号、终止行号(从0开始算)
     * @return void
     */
    public static void drawBarChart(XSSFSheet sheet, BarDirection direction, List<String> titles, List<Integer> rowCols,
                                    String[] categoryValue, Integer[] dataValue) {
        // 创建一个画布
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        // 画布占的行数和列数
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
        // 创建一个chart对象
        XSSFChart chart = drawing.createChart(anchor);
        // 标题
        chart.setTitleText(titles.get(0));
        // 标题覆盖
        chart.setTitleOverlay(false);
        // 图例位置
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP);

        // 分类轴标(X轴),标题位置
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(BarDirection.BAR.equals(direction)?AxisPosition.LEFT: AxisPosition.BOTTOM);
        bottomAxis.setTitle(titles.get(1));
        // 值(Y轴)轴,标题位置
        XDDFValueAxis leftAxis = chart.createValueAxis(BarDirection.BAR.equals(direction)?AxisPosition.BOTTOM:AxisPosition.LEFT);
        leftAxis.setTitle(titles.get(2));


        // CellRangeAddress(起始行号，终止行号， 起始列号，终止列号）
        // 分类轴标(X轴)数据，单元格范围位置
        //XDDFDataSource<String> category = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, 1, 0, 0));
        XDDFCategoryDataSource category = XDDFDataSourcesFactory.fromArray(categoryValue);
        // 数据1，单元格范围位置
        //XDDFNumericalDataSource<Double> data = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, 1, 1, 1));
        XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataValue);
        // bar：条形图，
        XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
        // 设置为可变颜色
        bar.setVaryColors(false);// 如果需要设置成自己想要的颜色，这里可变颜色要设置成false
        // 条形图方向，纵向/横向：纵向
        bar.setBarDirection(direction);
        // 图表加载数据，条形图1
        XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data);
        // 条形图例标题
        series1.setTitle("", null);
        XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
        // 条形图，填充颜色
        series1.setFillProperties(fill);

        //添加网格线
        chart.getCTChart().getPlotArea().getValAxArray(0).addNewMajorGridlines();
        // 绘制
        chart.plot(bar);
    }


    /**
     * @Description 画双柱状图
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 17:05
     * @param sheet excel页签
     * @param rowCols 画图位置:起始列号、起始行号、终止列号、终止行号
     * @param titles 标题:总标题、X轴标题、Y轴标题、条形图1标题、条形图2标题
     * @param categoryValue 类别数据
     * @param firstDataValue 条形图1具体数据
     * @param secDataValue 条形图2具体数据
     * @return void
     */
    public static void drawDoubleBarChart(XSSFSheet sheet, List<Integer> rowCols, List<String> titles,
                                          String[] categoryValue, Integer[] firstDataValue, Integer[] secDataValue) {

        // 创建一个画布
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        // 设置位置
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
                rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
        // 创建一个chart对象
        XSSFChart chart = drawing.createChart(anchor);
        // 标题
        chart.setTitleText(titles.get(0));
        // 标题覆盖
        chart.setTitleOverlay(false);
        // 图例位置
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP);
        // 分类轴标(X轴),标题位置
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        bottomAxis.setTitle(titles.get(1));
        // 值(Y轴)轴,标题位置
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle(titles.get(2));

        XDDFCategoryDataSource category = XDDFDataSourcesFactory.fromArray(categoryValue);
        //条形图1具体数据
        XDDFNumericalDataSource<Integer> data1 = XDDFDataSourcesFactory.fromArray(firstDataValue);
        //条形图2具体数据
        XDDFNumericalDataSource<Integer> data2 = XDDFDataSourcesFactory.fromArray(secDataValue);
        // bar：条形图，
        XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);

        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
        // 设置为可变颜色
        bar.setVaryColors(true);
        // 条形图方向，纵向/横向：纵向
        bar.setBarDirection(BarDirection.COL);

        // 图表加载数据，条形图1
        XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data1);
        // 条形图例标题
        series1.setTitle(titles.get(3), null);
        XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
        // 条形图，填充颜色
        series1.setFillProperties(fill);

        // 图表加载数据，条形图2
        XDDFBarChartData.Series series2 = (XDDFBarChartData.Series) bar.addSeries(category, data2);
        // 条形图例标题
        series2.setTitle(titles.get(4), null);
        XDDFSolidFillProperties fill2 = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.ORANGE));
        // 条形图，填充颜色
        series2.setFillProperties(fill2);
        // 绘制
        chart.plot(bar);
    }

    /**
     * @Description 画饼图
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/12/22 16:35
     * @param sheet excel页签
     * @param rowCols 画图位置:起始列号、起始行号、终止列号、终止行号
     * @param title 标题
     * @param categoryValue 类别数据
     * @param dataValue 具体数据
     * @return void
     */
    public static void drawPieChart(XSSFSheet sheet, String title, List<Integer> rowCols,
                                    String[] categoryValue, Integer[] dataValue) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0,
                rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
        XSSFChart chart = drawing.createChart(anchor);
        chart.setTitleText(title);
        chart.setTitleOverlay(false);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);

        XDDFDataSource<String> category = XDDFDataSourcesFactory.fromArray(categoryValue);
        // 数据1，单元格范围位置
        //XDDFNumericalDataSource<Double> data = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, 1, 1, 1));
        XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataValue);

        XDDFChartData chartData = chart.createData(ChartTypes.PIE, null, null);
        chartData.setVaryColors(true);
        chartData.addSeries(category, data);
        chart.plot(chartData);
    }


}
