package com.xiechy.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * POI EXCEL 图表-柱状图单柱状图(竖)
 * ————————————————
 * 版权声明：本文为CSDN博主「小百菜」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/u014644574/article/details/105695787
 */
public class SingleBarChart {
 
	public static void main(String[] args) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		String sheetName = "Sheet1";
		FileOutputStream fileOut = null;
		try {
			XSSFSheet sheet = wb.createSheet(sheetName);
			// 第一行:应用按键响应耗时
			Row row = sheet.createRow(0);
			Cell cell = row.createCell(0);
			cell.setCellValue("应用按键响应耗时");
			cell = row.createCell(1);
			cell.setCellValue(700);

			// 第二行:player/mediacodec解码首帧耗时
			row = sheet.createRow(1);
			cell = row.createCell(0);
			cell.setCellValue("player/mediacodec解码首帧耗时");
			cell = row.createCell(1);
			cell.setCellValue(650);
			// 创建一个画布
			XSSFDrawing drawing = sheet.createDrawingPatriarch();
			// 14行，5列
			XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 3, 6, 17);
			// 创建一个chart对象
			XSSFChart chart = drawing.createChart(anchor);
			// 标题
			chart.setTitleText("各模块最终平均耗时分布");
			// 标题覆盖
			chart.setTitleOverlay(false);
			// 图例位置
			XDDFChartLegend legend = chart.getOrAddLegend();
			legend.setPosition(LegendPosition.TOP);
 
			// 分类轴标(X轴),标题位置
			XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
			bottomAxis.setTitle("模块");
			// 值(Y轴)轴,标题位置
			XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
			leftAxis.setTitle("耗时");
 
			// CellRangeAddress(起始行号，终止行号， 起始列号，终止列号）
			// 分类轴标(X轴)数据，单元格范围位置
			XDDFDataSource<String> category = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, 1, 0, 0));
			// XDDFCategoryDataSource countries = XDDFDataSourcesFactory.fromArray(new String[] {"俄罗斯","加拿大","美国","中国","巴西","澳大利亚","印度"});
			// 数据1，单元格范围位置
			XDDFNumericalDataSource<Double> data = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, 1, 1, 1));
			// XDDFNumericalDataSource<Integer> area = XDDFDataSourcesFactory.fromArray(new Integer[] {17098242,9984670,9826675,9596961,8514877,7741220,3287263});
 
			// bar：条形图，
			XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
 
			leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
			// 设置为可变颜色
			bar.setVaryColors(false);// 如果需要设置成自己想要的颜色，这里可变颜色要设置成false
			// 条形图方向，纵向/横向：纵向
			bar.setBarDirection(BarDirection.COL);
 
			// 图表加载数据，条形图1
			XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data);
			// 条形图例标题
			series1.setTitle("各模块最终平均耗时分布", null);
			XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
			// 条形图，填充颜色
			series1.setFillProperties(fill);

			//添加网格线
			chart.getCTChart().getPlotArea().getValAxArray(0).addNewMajorGridlines();
 
			// 绘制
			chart.plot(bar);
			// CTBarSer ser = chart.getCTChart().getPlotArea().getBarChartArray(0).getSerArray(0);
			// CTLegend legend2 = chart.getCTChartSpace().getChart().getLegend();//更详细的图例设置
 
			// 打印图表的xml
			//System.out.println(chart.getCTChart());
 
			// 将输出写入excel文件
			String filename = "耗时分部.xlsx";
			fileOut = new FileOutputStream(filename);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wb.close();
			if (fileOut != null) {
				fileOut.close();
			}
		}
 
	}
 
}
