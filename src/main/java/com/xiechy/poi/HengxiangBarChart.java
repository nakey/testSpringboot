package com.xiechy.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.PresetColor;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * POI EXCEL 图表-柱状图单柱状图(横向)
 * ————————————————
 * 版权声明：本文为CSDN博主「小百菜」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/u014644574/article/details/105695787
 */
public class HengxiangBarChart {
 
	public static void main(String[] args) throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook();
		String sheetName = "Sheet1";
		FileOutputStream fileOut = null;

		List<Integer> rowCal =Arrays.asList(new Integer[]{0,3,9,20});
		List<String> titles = Arrays.asList(new String[]{"各应用平均起播速度","应用","时间"});
		String[] category ={"斗鱼","腾讯vod","虎牙","小电视","奇异果","酷喵"};
		Integer[] dataVaue = {1300,900,1200,1100,1000,1400};

		try {
			XSSFSheet sheet = wb.createSheet(sheetName);
			drawBarChart(sheet,BarDirection.BAR,titles,category, dataVaue,rowCal);
			// 将输出写入excel文件
			String filename = "平均速度1.xlsx";
			fileOut = new FileOutputStream(filename);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wb.close();
			if (fileOut != null) {
				fileOut.close();
			}
		}
 
	}


	/**
	 * @Description 画单柱状图（可横可竖）
	 * @Author R&D-VAL SZ nakey.xie
	 * @Date  2021/12/22 15:03
	 * @param sheet excel页
	 * @param direction col为竖向,bar为横向
	 * @param titles 总标题，x轴标题,轴标题，不设置填写空串
	 * @param categoryValue x轴类别
	 * @param dataValue 具体数值
	 * @param rowCols 柱状图位置:起始列号、起始行号、终止列号、终止行号
	 * @return void
	*/
	public static void drawBarChart(XSSFSheet sheet, BarDirection direction, List<String> titles, String[] categoryValue, Integer[] dataValue, List<Integer> rowCols) {
		// 创建一个画布
		XSSFDrawing drawing = sheet.createDrawingPatriarch();
		// 画布占的行数和列数
		XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, rowCols.get(0), rowCols.get(1), rowCols.get(2), rowCols.get(3));
		// 创建一个chart对象
		XSSFChart chart = drawing.createChart(anchor);
		// 标题
		chart.setTitleText(titles.get(0));
		// 标题覆盖
		chart.setTitleOverlay(false);
		// 图例位置
		XDDFChartLegend legend = chart.getOrAddLegend();
		legend.setPosition(LegendPosition.TOP);

		// 分类轴标(X轴),标题位置
		XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(BarDirection.BAR.equals(direction)?AxisPosition.LEFT: AxisPosition.BOTTOM);
		bottomAxis.setTitle(titles.get(1));
		// 值(Y轴)轴,标题位置
		XDDFValueAxis leftAxis = chart.createValueAxis(BarDirection.BAR.equals(direction)?AxisPosition.BOTTOM:AxisPosition.LEFT);
		leftAxis.setTitle(titles.get(2));


		// CellRangeAddress(起始行号，终止行号， 起始列号，终止列号）
		// 分类轴标(X轴)数据，单元格范围位置
		//XDDFDataSource<String> category = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(0, 1, 0, 0));
		XDDFCategoryDataSource category = XDDFDataSourcesFactory.fromArray(categoryValue);
		// 数据1，单元格范围位置
		//XDDFNumericalDataSource<Double> data = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(0, 1, 1, 1));
		XDDFNumericalDataSource<Integer> data = XDDFDataSourcesFactory.fromArray(dataValue);
		// bar：条形图，
		XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
		leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
		// 设置为可变颜色
		bar.setVaryColors(false);// 如果需要设置成自己想要的颜色，这里可变颜色要设置成false
		// 条形图方向，纵向/横向：纵向
		bar.setBarDirection(direction);
		// 图表加载数据，条形图1
		XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(category, data);
		// 条形图例标题
		series1.setTitle("", null);
		XDDFSolidFillProperties fill = new XDDFSolidFillProperties(XDDFColor.from(PresetColor.LIGHT_BLUE));
		// 条形图，填充颜色
		series1.setFillProperties(fill);

		//添加网格线
		chart.getCTChart().getPlotArea().getValAxArray(0).addNewMajorGridlines();
		// 绘制
		chart.plot(bar);
	}
 
}
