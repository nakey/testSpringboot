/*
package com.xiechy.util;

import cn.llc.exception.ServiceException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.Random;


*/
/**
 * @Description: 阿里云OSS服务器工具类
 * @author: Yangxf
 * @date: 2019/4/18 16:26
 *//*

@Component
public class OssUtil {


    protected static final Logger logger = LoggerFactory.getLogger(OssUtil.class);

    //阿里云OSS地址，这里看根据你的oss选择
    //内网地址上传
    @Value("${aliyun.endpoint}")
    private String endpoint ;
    //外网地址访问
    @Value("${aliyun.endpointExtranet}")
    private String endpointExtranet ;
    //阿里云OSS账号
    @Value("${aliyun.accessKeyId}")
    private String accessKeyId ;
    //阿里云OSS密钥
    @Value("${aliyun.accessKeySecret}")
    private String accessKeySecret;
    //阿里云OSS上的存储块bucket名字
    @Value("${aliyun.bucketName}")
    private String bucketName;
    //阿里云图片文件存储目录
    @Value("${aliyun.homeimagedir}")
    private String homeimagedir ;
    */
/**
     * 固件升级包存储目录
     *//*

    @Value("${aliyun.upgradeDir}")
    private String upgradeDir;
    */
/**
     * 自有域名
     *//*

    @Value("${aliyun.llcDomain}")
    private String llcDomain;

    private OSSClient ossClient;

    private OSSClient ossClientExtranet;

    */
/**
     * 文件类型： 图片
     *//*

    private final int FILE_TYPE_IMAGE = 1;

    */
/**
     * 文件类型： 升级包
     *//*

    private final int FILE_TYPE_UPGRADE = 2;


    @PostConstruct
    public void OssUtil(){
        ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        ossClientExtranet = new OSSClient(endpointExtranet, accessKeyId, accessKeySecret);
    }



    */
/**
     * 文件 上传阿里云oss
     *
     * @param file
     * @return
     *//*

    private String uploadFileOSS(MultipartFile file, int fileType) {
//        if (file.getSize() > 1024 * 1024 * 3) {
//            throw new RuntimeException("上传图片大小不能超过3M！");
//        }
        String originalFilename = file.getOriginalFilename();
        String substring = originalFilename.substring(originalFilename.lastIndexOf(".")).toLowerCase();
        Random random = new Random();
        String randomFileName = random.nextInt(10000) + System.currentTimeMillis() + substring;
        try {
            InputStream inputStream = file.getInputStream();
            this.uploadFileOSS(inputStream, randomFileName, fileType);
            return randomFileName;
        } catch (Exception e) {
            logger.error("uploadFileOSS-fail:{}", e.getMessage(), e);
            throw new ServiceException("上传图片失败!");
        }
    }

    */
/**
     * 图片 上传阿里云oss
     *
     * @param file
     * @return
     *//*

    public String uploadHomeImageOSS(MultipartFile file) {
        return uploadFileOSS(file, FILE_TYPE_IMAGE);
    }

    */
/**
     * 升级包 上传阿里云oss
     *
     * @param file
     * @return
     *//*

    public String uploadUpgradeFileOSS(MultipartFile file) {
        return uploadFileOSS(file, FILE_TYPE_UPGRADE);
    }


    */
/**
     * 获得升级包路径
     *
     * @param fileUrl
     * @return
     *//*

    public String getUpgradeFileUrl(String fileUrl) {
        if (!StringUtils.isEmpty(fileUrl)) {
            return llcDomain + upgradeDir + fileUrl;
        }
        return null;
    }

    */
/**
     * 获得图片路径
     *
     * @param fileUrl
     * @return
     *//*

    public String getHomeImageUrl(String fileUrl) {
        if (!StringUtils.isEmpty(fileUrl)) {
            String[] split = fileUrl.split("/");
            return this.getUrl(this.homeimagedir + split[split.length - 1]);
        }
        return null;
    }

    */
/**
     * 图片上传到OSS服务器  如果同名文件会覆盖服务器上的
     *
     * @param instream 文件流
     * @param fileName 文件名称 包括后缀名
     * @return 出错返回"" ,唯一MD5数字签名
     *//*

    private String uploadFileOSS(InputStream instream, String fileName, int fileType) {
        String ret = "";
        try {
            //创建上传Object的Metadata
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(instream.available());
            objectMetadata.setCacheControl("no-cache");
            objectMetadata.setHeader("Pragma", "no-cache");
            objectMetadata.setContentType(getcontentType(fileName.substring(fileName.lastIndexOf("."))));
            objectMetadata.setContentDisposition("inline;filename=" + fileName);

            String path = homeimagedir + fileName;

            if (fileType == FILE_TYPE_UPGRADE) {
                path = upgradeDir + fileName;
            }
            //上传文件
            PutObjectResult putResult = ossClient.putObject(bucketName, path, instream, objectMetadata);
            ret = putResult.getETag();
            return ret;
        } catch (Exception e) {
            logger.error("uploadHomeImageFileOSS-fail:{}", e.getMessage(), e);
            throw new ServiceException("上传oss图片失败!");
        } finally {
            try {
                if (instream != null) {
                    instream.close();
                }
            } catch (IOException e) {
                logger.error("关闭资源异常", e);
            }
        }
    }

    */
/**
     * 判断OSS服务文件上传时文件的类型contentType
     *
     * @param FilenameExtension 文件后缀
     * @return String
     *//*

    public static String getcontentType(String FilenameExtension) {
        if (FilenameExtension.equalsIgnoreCase(".bmp")) {
            return "image/bmp";
        }
        if (FilenameExtension.equalsIgnoreCase(".gif")) {
            return "image/gif";
        }
        if (FilenameExtension.equalsIgnoreCase(".jpeg") ||
                FilenameExtension.equalsIgnoreCase(".jpg") ||
                FilenameExtension.equalsIgnoreCase(".png")) {
            return "image/jpeg";
        }
        if (FilenameExtension.equalsIgnoreCase(".html")) {
            return "text/html";
        }
        if (FilenameExtension.equalsIgnoreCase(".txt")) {
            return "text/plain";
        }
        if (FilenameExtension.equalsIgnoreCase(".vsd")) {
            return "application/vnd.visio";
        }
        if (FilenameExtension.equalsIgnoreCase(".pptx") ||
                FilenameExtension.equalsIgnoreCase(".ppt")) {
            return "application/vnd.ms-powerpoint";
        }
        if (FilenameExtension.equalsIgnoreCase(".docx") ||
                FilenameExtension.equalsIgnoreCase(".doc")) {
            return "application/msword";
        }
        if (FilenameExtension.equalsIgnoreCase(".xml")) {
            return "text/xml";
        }
        if (FilenameExtension.equalsIgnoreCase(".bin")) {
            return "application/octet-stream";
        }
        if (FilenameExtension.equalsIgnoreCase(".rbl")) {
            return "application/octet-stream";
        }
        return "image/jpg";
    }

    */
/**
     * 获得url链接
     *
     * @param key
     * @return
     *//*

    public String getUrl(String key) {
        // 设置URL过期时间为10年  3600l* 1000*24*365*10
        Date expiration = new Date(new Date().getTime() + 3600L * 1000 * 24 * 365 * 10);
        // 生成URL
        URL url = ossClientExtranet.generatePresignedUrl(bucketName, key, expiration);
        if (url != null) {
            return url.toString();
        }
        return null;
    }

    */
/**
     * 判断图片
     *
     * @param file
     * @return
     * @throws Exception
     *//*

    public  String updateHomeImage(MultipartFile file)  {
        if (file == null || file.getSize() <= 0) {
            throw new RuntimeException("图片不能为空");
        }
        String name = uploadHomeImageOSS(file);
        String imgUrl = getHomeImageUrl(name);
        return imgUrl;
    }


    public String getEndpoint() {
        return endpoint;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public String getAccessKeySecret() {
        return accessKeySecret;
    }

    public String getBucketName() {
        return bucketName;
    }

    public String getHomeimagedir() {
        return homeimagedir;
    }

    public String getEndpointExtranet() {
        return endpointExtranet;
    }

    public String getUpgradeDir() {
        return upgradeDir;
    }

    public void setUpgradeDir(String upgradeDir) {
        this.upgradeDir = upgradeDir;
    }
}
*/
