package com.xiechy.util;

import cn.llc.common.contants.SysContants;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName HttpClientUtil
 * @Description http调用
 * @Author ymy
 * @Date 2020/10/17 14:07
 */
public class HttpClientUtil {

    private static Logger log = LoggerFactory.getLogger(HttpClientUtil.class);

    /**
     * @param url param
     * @return com.alibaba.fastjson.JSONObject
     * @Description post请求，参数为json格式
     * @Author ymy
     * @Date 2020/10/17 14:08
     */
    public static String PostJsonParam(String url, Map<String, Object> param) {
        log.info("远程调用第三方服务，url：{},请求参数：{}", url, JSON.toJSONString(param));
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().set(SysContants.NUMBER.ONE,new StringHttpMessageConverter(StandardCharsets.UTF_8));
        MediaType type = MediaType.parseMediaType("application/json;charset=UTF-8");
        HttpHeaders headers = new HttpHeaders();
        //定义请求参数类型，这里用json所以是MediaType.APPLICATION_JSON
        headers.setContentType(type);
        //RestTemplate带参传的时候要用HttpEntity<?>对象传递
        HttpEntity<Map<String, Object>> request = new HttpEntity<Map<String, Object>>(param, headers);
        ResponseEntity<String> entity = restTemplate.postForEntity(url, request, String.class);
        //获取3方接口返回的数据通过entity.getBody();它返回的是一个字符串；
        String body = entity.getBody();
        return body;
    }

    /**
     * @param domian path
     * @return java.lang.String
     * @Description 请求路径拼接
     * @Author ymy
     * @Date 2020/10/19 14:53
     */
    public static String joinUrl(String domian, String path) {
        if(!domian.startsWith("http://") && !domian.startsWith("https://") ){
            domian = "http://" + domian;
        }
        if (!domian.endsWith("/") && !path.startsWith("/")) {
            return domian + "/" + path;
        }
        return domian + path;
    }

}
