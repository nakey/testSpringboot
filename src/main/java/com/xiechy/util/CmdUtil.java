package com.xiechy.util;



import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author hundanli
 * @version 1.0.0
 * @date 2021/1/11 13:54
 */
@Slf4j
public class CmdUtil {

    private CmdUtil() {

    }

    private static final Integer TIMEOUT = 20;

    /**
     * 执行系统命令
     * @param workDir 工作目录
     * @param commands 命令
     * @return 命令执行输出
     */
    public static List<String> runCommand(String workDir, String... commands) {
        ProcessBuilder builder = new ProcessBuilder(commands);
        builder.directory(new File(workDir));
        builder.redirectErrorStream(true);
        BufferedReader reader = null;
        Process process = null;

        try {
            process = builder.start();
            if (!process.waitFor(TIMEOUT, TimeUnit.SECONDS)) {
                throw new RuntimeException("cmd命令执行超时");
            }
            log.info("cmd命令返回值 exitValue={}", process.exitValue());
            if (process.exitValue() != 0) {
                // error output
                InputStream errorStream = process.getErrorStream();
                reader = new BufferedReader(new InputStreamReader(errorStream));
                String errorOutput = reader.lines().collect(Collectors.joining());
                log.error("cmd命令执行出错：errorOutput={}", errorOutput);
                // std output
                InputStream inputStream = process.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                String stdOutput = reader.lines().collect(Collectors.joining());
                log.error("cmd命令执行出错：stdOutput={}", stdOutput);

                throw new RuntimeException( "cmd命令执行出错：" + stdOutput);

            }
            log.info("cmd命令执行成功...");
            reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "GBK"));
            return reader.lines().collect(Collectors.toList());
        } catch (Exception e) {
            log.error("cmd命令执行失败：{}, Error: {}"+ Arrays.toString(commands), e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    log.error("IO异常：" + e.fillInStackTrace());
                }
            }
            if (process != null) {
                process.destroy();
            }
        }
    }


    public static void main(String[] args) {
        String commands ="ipconfig";
        List<String> results = runCommand("/", commands);
        //results.stream().forEach(line->System.out.println(line));
        // 遍历results，并依次打印其中的元素
        results.forEach(System.out::println);
    }
}
