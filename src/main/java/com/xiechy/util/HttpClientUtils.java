/*
package com.xiechy.util;

import cn.llc.common.enums.SysEnum;
import cn.llc.common.util.transfer.WXPayUtil;
import cn.llc.order.service.transfer.api.IWXPayDomain;
import cn.llc.order.service.transfer.api.WXPayConfig;
import cn.llc.order.service.transfer.api.WXPayReport;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


*/
/**
 * @author xdd
 * @date 2018/9/13 16:40
 * @description http https 请求工具
 *//*

public final class HttpClientUtils {
    private static final String CHART_SET = "UTF-8";

    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtils.class);

    public static String post(String url, String xml) {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建httppost
        HttpPost httppost = new HttpPost(url);
        HttpEntity eitity;
        try {
            eitity = new ByteArrayEntity(xml.getBytes("utf-8"), ContentType.TEXT_XML);
            httppost.setEntity(eitity);
            logger.info("executing request " + httppost.getURI());
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String content = EntityUtils.toString(entity, "utf-8");
                    return content;
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            logger.error("发送post请求异常：", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("发送post请求异常：", e);
        } catch (IOException e) {
            logger.error("发送post请求异常：", e);
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                logger.error("关闭post请求异常：", e);
            }
        }
        return "";
    }

    public static String post(String url, String xml,ContentType contentType) {
        // 创建默认的httpClient实例.
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建httppost
        HttpPost httppost = new HttpPost(url);
        HttpEntity eitity;
        try {
            eitity = new ByteArrayEntity(xml.getBytes("utf-8"), contentType);
            httppost.setEntity(eitity);
            logger.info("executing request " + httppost.getURI());
            CloseableHttpResponse response = httpclient.execute(httppost);
            try {
                HttpEntity entity = response.getEntity();
                if (entity != null) {
                    String content = EntityUtils.toString(entity, "utf-8");
                    return content;
                }
            } finally {
                response.close();
            }
        } catch (ClientProtocolException e) {
            logger.error("发送post请求异常：", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("发送post请求异常：", e);
        } catch (IOException e) {
            logger.error("发送post请求异常：", e);
        } finally {
            // 关闭连接,释放资源
            try {
                httpclient.close();
            } catch (IOException e) {
                logger.error("关闭post请求异常：", e);
            }
        }
        return "";
    }


    */
/**
     * 向指定 URL 发送POST方法的请求
     *
     * @param url
     *            发送请求的 URL
     * @param param
     *            请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     *//*

    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        StringBuilder result = new StringBuilder();
        if(url.indexOf("ocr_idcardocr") == -1){
            logger.info("Http请求地址："+url+",请求参数："+param);
        }
        try {
            URL realUrl = new URL(url);
            URLConnection conn = realUrl.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            out.print(param);
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(conn.getInputStream(),"utf-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result.append(line) ;
            }
        } catch (Exception e) {
            logger.error("发送请求失败，原因：", e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                logger.error("发送请求失败，原因：", ex);
            }
        }
        if(url.indexOf("ocr_idcardocr") == -1){
            logger.info("Http接收参数："+result);
        }
        return result.toString();
    }

    public static String sendGet(String url, String params) {
        String readResult = null;
        URLConnection connection = null;
        String URL = url + "?" + params;
        logger.info(URL);
        BufferedReader br = null;
        try {
            connection = new URL(URL).openConnection();
            HttpURLConnection con = (HttpURLConnection) connection;
            con.setConnectTimeout(15000);
            con.setReadTimeout(15000);
            con.connect();
            int status = con.getResponseCode();
            if (HttpURLConnection.HTTP_OK == status) {
                InputStream fin = con.getInputStream();
                br = new BufferedReader(new InputStreamReader(fin, "utf-8"));
                StringBuffer buffer = new StringBuffer();
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    buffer.append(temp);
                }

                readResult = buffer.toString();
                logger.info(readResult);
                return readResult;
            }
        } catch (SocketTimeoutException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (UnknownHostException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (IOException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (Exception e) {
            logger.error("发送请求失败，原因：", e);
        }finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    logger.error("发送请求失败，原因：", e);
                }
            }
        }
        return null;
    }

    
    public static String sendGet(String url, Map<String,Object> params) {
        String readResult = null;
        URLConnection connection = null;
        StringBuffer requestUrl = new StringBuffer();

        BufferedReader br = null;
        try {
        	requestUrl.append(url);
        	if(params!=null)
        	{
        		for(Map.Entry<String, Object> entry : params.entrySet())
        		{
	           		 String mapKey = entry.getKey();
	            	 Object mapValue = entry.getValue();
	           		 requestUrl.append("?");
	           		 requestUrl.append(mapKey).append("=").append(mapValue).append("&");
           	    }
        	}
        	
            logger.info(requestUrl.toString());
            connection = new URL(requestUrl.toString()).openConnection();
            HttpURLConnection con = (HttpURLConnection) connection;
            con.setConnectTimeout(15000);
            con.setReadTimeout(15000);
            con.connect();
            int status = con.getResponseCode();
            if (HttpURLConnection.HTTP_OK == status) {
                InputStream fin = con.getInputStream();
                br = new BufferedReader(new InputStreamReader(fin, "utf-8"));
                StringBuffer buffer = new StringBuffer();
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    buffer.append(temp);
                }

                readResult = buffer.toString();
                logger.info(readResult);
                return readResult;
            }
        } catch (SocketTimeoutException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (UnknownHostException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (IOException e) {
            logger.error("发送请求失败，原因：", e);
        } catch (Exception e) {
            logger.error("发送请求失败，原因：", e);
        }finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    logger.error("发送请求失败，原因：", e);
                }
            }
        }
        return null;
    }

    */
/**
     *
     *
     * @Title:sendPostByJson:(方法名).
     * @Description:JSON字符串 CONTENT_TYPE:application/json
     * @Date: 2018年1月10日 下午4:32:17
     * @param urlAddress
     * @param param
     * @return
     *//*

    public static String sendPostByJson(String urlAddress, String param) {
        StringBuffer result = new StringBuffer("");
        logger.info("Http请求地址："+urlAddress+",请求参数："+param);
        try {

            URL url = new URL(urlAddress);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Charset", CHART_SET);

            byte[] data = param.getBytes();
            connection.setRequestProperty("Content-Length", String.valueOf(data.length));
            connection.connect();

            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.write(param.toString().getBytes(CHART_SET));
            out.flush();
            out.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), CHART_SET));
            String lines;

            while ((lines = reader.readLine()) != null) {
                result.append(lines);
            }

            reader.close();
            connection.disconnect();


        } catch (Exception e) {
            logger.error("发送请求失败，原因：", e);
        }
        logger.info("Http接收参数："+result.toString());
        return result.toString();
    }

    public static String postJSON(String urlAddress, String json) throws Exception{

        StringBuffer result = new StringBuffer("");

        URL url = new URL(urlAddress);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Charset", "UTF-8");

        byte[] data = json.getBytes();
        connection.setRequestProperty("Content-Length", String.valueOf(data.length));
        connection.connect();

        DataOutputStream out = new DataOutputStream(connection.getOutputStream());
        out.write(json.toString().getBytes("UTF-8"));
        out.flush();
        out.close();

        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String lines;

        while ((lines = reader.readLine()) != null) {
            result.append(lines);
        }

        reader.close();
        connection.disconnect();

        return result.toString();
    }


    */
/**
     *
     *
     * @Title:sendPostByFormData:(方法名).
     * @Description:表单数据类型CONTENT_TYPE:multipart/form-data
     * @Date: 2018年1月10日 下午4:32:51
     * @param url
     * @param param
     * @return
     *//*

    public static String sendPostByFormData(String url, List<BasicNameValuePair> param) {
        String result = "";
        logger.info("Http请求地址："+url+",请求参数："+param);
        try {
            HttpClient httpClient = HttpClients. createDefault();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(param,"utf-8"));
            HttpResponse response=httpClient.execute(httpPost);
            if(response != null && response.getStatusLine().getStatusCode() == 200) {
                result=EntityUtils.toString(response.getEntity());
            }
        } catch (Exception e) {
            logger.error("发送请求失败，原因：", e);
        }
        logger.info("Http接收参数："+result);
        return result;
    }

    */
/**
     * 请求webservice
     * @param url
     * @param xml
     * @return
     *//*

    public static String postWs(String url,String xml) throws Exception{
        OutputStream os = null;
        InputStream is = null;
        HttpURLConnection conn = null;
        try {
            URL wsUrl = new URL(url);
            conn = (HttpURLConnection) wsUrl.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
            os = conn.getOutputStream();
            os.write(xml.getBytes());
            is = conn.getInputStream();

            byte[] b = new byte[1024];
            int len = 0;
            StringBuilder result = new StringBuilder();
            while((len = is.read(b)) != -1){
                String s = new String(b,0,len,"UTF-8");
                result.append(s);
            }
            return result.toString();
        } finally {
            if(conn != null) {
                conn.disconnect();
            }
            if(is != null) {
                is.close();
            }
            if(os != null) {
                os.close();
            }

        }
    }

    */
/**
     * 发送https请求
     * @param requestUrl 请求地址
     * @param requestMethod 请求方式（GET、POST）
     * @param outputStr 提交的数据
     * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
     *//*

    public static String httpsRequest(String requestUrl, String requestMethod, String outputStr) {
        String json ="";
        try {
            // 创建SSLContext对象，并使用我们指定的信任管理器初始化
            TrustManager[] tm = { new MyX509TrustManager() };
            SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
            sslContext.init(null, tm, new SecureRandom());
            // 从上述SSLContext对象中得到SSLSocketFactory对象
            SSLSocketFactory ssf = sslContext.getSocketFactory();

            URL url = new URL(requestUrl);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(ssf);

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            // 设置请求方式（GET/POST）
            conn.setRequestMethod(requestMethod);

            // 当outputStr不为null时向输出流写数据
            if (null != outputStr) {
                OutputStream outputStream = conn.getOutputStream();
                // 注意编码格式
                outputStream.write(outputStr.getBytes("UTF-8"));
                outputStream.close();
            }

            // 从输入流读取返回内容
            InputStream inputStream = conn.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "utf-8");
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String str = null;
            StringBuffer buffer = new StringBuffer();
            while ((str = bufferedReader.readLine()) != null) {
                buffer.append(str);
            }

            // 释放资源
            bufferedReader.close();
            inputStreamReader.close();
            inputStream.close();
            inputStream = null;
            conn.disconnect();
            logger.info("小程序模板消息请求结果："+buffer.toString());
            System.out.println("小程序模板消息请求结果："+buffer.toString());
            json = buffer.toString();
        } catch (ConnectException ce) {
            System.out.println("连接超时");
        } catch (Exception e) {
            System.out.println("请求异常");
        }
        return json;
    }
    


    */
/***
     * 请求工具类
     * @param urlSuffix
     * @param data
     * @param useCert
     * @param config
     * @return
     * @throws Exception
     *//*

    public static String request(String urlSuffix, String data, boolean useCert, WXPayConfig config,Integer type) throws Exception {
        Exception exception = null;
        long elapsedTimeMillis = 0;
        long startTimestampMs = WXPayUtil.getCurrentTimestampMs();
        boolean firstHasDnsErr = false;
        boolean firstHasConnectTimeout = false;
        boolean firstHasReadTimeout = false;
        String uuid = UuidUtil.getUid();
        int connectTimeoutMs = config.getHttpConnectTimeoutMs();
        int readTimeoutMs = config.getHttpReadTimeoutMs();
        IWXPayDomain.DomainInfo domainInfo = config.getWXPayDomain().getDomain(config);
        if (domainInfo == null) {
            throw new Exception("WXPayConfig.getWXPayDomain().getDomain() is empty or null");
        }
        try {
            String result = requestOnce(domainInfo.domain, urlSuffix, uuid, data, connectTimeoutMs, readTimeoutMs, useCert,config,type);
            elapsedTimeMillis = WXPayUtil.getCurrentTimestampMs() - startTimestampMs;
            config.getWXPayDomain().report(domainInfo.domain, elapsedTimeMillis, null);
            WXPayReport.getInstance(config).report(
                    uuid,
                    elapsedTimeMillis,
                    domainInfo.domain,
                    domainInfo.primaryDomain,
                    connectTimeoutMs,
                    readTimeoutMs,
                    firstHasDnsErr,
                    firstHasConnectTimeout,
                    firstHasReadTimeout);
            return result;
        } catch (UnknownHostException ex) {  // dns 解析错误，或域名不存在
            exception = ex;
            firstHasDnsErr = true;
            elapsedTimeMillis = WXPayUtil.getCurrentTimestampMs() - startTimestampMs;
            WXPayUtil.getLogger().warn("UnknownHostException for domainInfo {}", domainInfo);
            WXPayReport.getInstance(config).report(
                    uuid,
                    elapsedTimeMillis,
                    domainInfo.domain,
                    domainInfo.primaryDomain,
                    connectTimeoutMs,
                    readTimeoutMs,
                    firstHasDnsErr,
                    firstHasConnectTimeout,
                    firstHasReadTimeout
            );
        } catch (ConnectTimeoutException ex) {
            exception = ex;
            firstHasConnectTimeout = true;
            elapsedTimeMillis = WXPayUtil.getCurrentTimestampMs() - startTimestampMs;
            WXPayUtil.getLogger().warn("connect timeout happened for domainInfo {}", domainInfo);
            WXPayReport.getInstance(config).report(
                    uuid,
                    elapsedTimeMillis,
                    domainInfo.domain,
                    domainInfo.primaryDomain,
                    connectTimeoutMs,
                    readTimeoutMs,
                    firstHasDnsErr,
                    firstHasConnectTimeout,
                    firstHasReadTimeout
            );
        } catch (SocketTimeoutException ex) {
            exception = ex;
            firstHasReadTimeout = true;
            elapsedTimeMillis = WXPayUtil.getCurrentTimestampMs() - startTimestampMs;
            WXPayUtil.getLogger().warn("timeout happened for domainInfo {}", domainInfo);
            WXPayReport.getInstance(config).report(
                    uuid,
                    elapsedTimeMillis,
                    domainInfo.domain,
                    domainInfo.primaryDomain,
                    connectTimeoutMs,
                    readTimeoutMs,
                    firstHasDnsErr,
                    firstHasConnectTimeout,
                    firstHasReadTimeout);
        } catch (Exception ex) {
            exception = ex;
            elapsedTimeMillis = WXPayUtil.getCurrentTimestampMs() - startTimestampMs;
            WXPayReport.getInstance(config).report(
                    uuid,
                    elapsedTimeMillis,
                    domainInfo.domain,
                    domainInfo.primaryDomain,
                    connectTimeoutMs,
                    readTimeoutMs,
                    firstHasDnsErr,
                    firstHasConnectTimeout,
                    firstHasReadTimeout);
        }
        config.getWXPayDomain().report(domainInfo.domain, elapsedTimeMillis, exception);
        throw exception;
    }

    */
/**
     * 请求，只请求一次，不做重试
     *
     * @param domain
     * @param urlSuffix
     * @param uuid
     * @param data
     * @param connectTimeoutMs
     * @param readTimeoutMs
     * @param useCert          是否使用证书，针对退款、撤销等操作
     * @return
     * @throws Exception
     *//*

    public static String requestOnce(final String domain, String urlSuffix, String uuid, String data,
                                     int connectTimeoutMs, int readTimeoutMs, boolean useCert,WXPayConfig config
            ,Integer type) throws Exception {
        BasicHttpClientConnectionManager connManager;
        String USER_AGENT="";
        if (useCert) {
            // 证书
            char[] password = null;
            InputStream certStream = null;
            if(SysEnum.NoticePlatform.USER.getCode() == type){
                password = config.getMchID().toCharArray();
                certStream = config.getCertStream();
            }else if(SysEnum.NoticePlatform.AGENT.getCode() == type){
                password = config.getAgentMchid().toCharArray();
                certStream = config.getAgentCertStream();
            }
            KeyStore ks = KeyStore.getInstance("PKCS12");

            ks.load(certStream, password);

            // 实例化密钥库 & 初始化密钥工厂
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, password);

            // 创建 SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), null, new SecureRandom());

            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(
                    sslContext,
                    new String[]{"TLSv1"},
                    null,
                    new DefaultHostnameVerifier());

            connManager = new BasicHttpClientConnectionManager(
                    RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("http", PlainConnectionSocketFactory.getSocketFactory())
                            .register("https", sslConnectionSocketFactory)
                            .build(),
                    null,
                    null,
                    null
            );
        } else {
            connManager = new BasicHttpClientConnectionManager(
                    RegistryBuilder.<ConnectionSocketFactory>create()
                            .register("http", PlainConnectionSocketFactory.getSocketFactory())
                            .register("https", SSLConnectionSocketFactory.getSocketFactory())
                            .build(),
                    null,
                    null,
                    null
            );
        }

        HttpClient httpClient = HttpClientBuilder.create()
                .setConnectionManager(connManager)
                .build();

        String url = "https://" + domain + urlSuffix;
        HttpPost httpPost = new HttpPost(url);

        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(readTimeoutMs).setConnectTimeout(connectTimeoutMs).build();
        httpPost.setConfig(requestConfig);

        StringEntity postEntity = new StringEntity(data, "UTF-8");
        httpPost.addHeader("Content-Type", "text/xml");
        httpPost.addHeader("User-Agent", USER_AGENT + " " + config.getMchID());
        httpPost.setEntity(postEntity);

        HttpResponse httpResponse = httpClient.execute(httpPost);
        HttpEntity httpEntity = httpResponse.getEntity();
        return EntityUtils.toString(httpEntity, "UTF-8");

    }


    // 测试企业微信-机器人告警
    public static void main(String[] args) throws Exception {
        String urlPath = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=87923f1e-bb9e-4f5d-818c-8ef60b669457";
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("msgtype", "text");

        Map<String, String> sonparamMap = new HashMap<String, String>();
        sonparamMap.put("content", "test");
        sonparamMap.put("mentioned_list", "@all");

        paramMap.put("text", sonparamMap);

        // 转成json
        String jsonString = JSONObject.toJSONString(paramMap);
        String response = HttpClientUtils.sendPostByJson(urlPath, jsonString);
        System.out.print(response);
    }

}

*/
