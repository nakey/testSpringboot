package com.xiechy.util;

import cn.llc.common.contants.SysContants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *@ClassName MoneySwitchUtil
 *@Description 金额转换工具类
 *@Author dongdong.xie
 *@Date 2020/7/9 17:06
*/
public class MoneySwitchUtil {

    protected static final Logger logger = LoggerFactory.getLogger(MoneySwitchUtil.class);

    /**
     * 元转分，确保price保留两位有效数字
     * @return
     */
    public static BigDecimal changeY2F(String price) throws Exception{
        logger.info("[元转分]入参={}",price);
        BigDecimal resultBranch = BigDecimal.ZERO;
        try{
            if(StringUtils.isNotEmpty(price)){
                resultBranch = new BigDecimal(price).multiply(new BigDecimal("100")).setScale(0);
                logger.info("[元转分]出参={}",resultBranch);
            }
        }catch (Exception e){
            logger.error("[changeY2F] error:{}",e);
            throw new Exception("changeY2F异常");
        }
        return resultBranch;
    }

    /**
     * 分转元，转换为bigDecimal在toString
     * @return
     */
    public static String changeF2Y(int price) throws Exception{
        String result = SysContants.NUMBER.ZERO + "";
        logger.info("[分转元]入参={}",price);
        try{
            result = BigDecimal.valueOf(Long.valueOf(price)).divide(new BigDecimal(100)).toString();
            logger.info("[分转元]出参={}",result);
        }catch (Exception e){
            logger.error("[changeF2Y] error:{}",e);
            throw new Exception("changeY2F异常");
        }
        return result;
    }

    /**
     * 分转元
     * @return
     */
    public static String trans2Y(int money) {
        String result = "...";
        try {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            BigDecimal bigDecimal = BigDecimal.valueOf(Long.valueOf(money)).divide(new BigDecimal(100));
            result = decimalFormat.format(bigDecimal);
        } catch (Exception e) {
            logger.error("[trans2Y] money:{}", money, e);
        }
        return result;
    }

    /**
     * 分转元
     * xcy
     * 2020-11-9 18:15:44
     * @return
     */
    public static String trans2Y(long money) {
        String result = "...";
        try {
            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            BigDecimal bigDecimal = BigDecimal.valueOf(money).divide(new BigDecimal(100));
            result = decimalFormat.format(bigDecimal);
        } catch (Exception e) {
            logger.error("[trans2Y] money:{}", money, e);
        }
        return result;
    }

    /**
     * @Description 元转分
     * @Author xcy
     * @Date  2020/8/27 9:41
     * @param money
     * @return java.lang.Integer
    */
    public static Integer trans2F(String money){
        Integer result = 0;
        try {
            BigDecimal bigDecimal = BigDecimal.ZERO;
            bigDecimal = new BigDecimal(money).multiply(new BigDecimal("100")).setScale(0);
            result = bigDecimal.intValue();
        } catch (Exception e) {
            logger.error("[trans2F] money:{}", money, e);
        }
        return result;
    }

    /**
      * @Description 分转元
      * @Author ymy
      * @Date  2020/11/25 10:55
      * @param money
      * @return java.lang.String
     */
    public static BigDecimal transToY(Integer money) {
        try {
            BigDecimal bigDecimal = BigDecimal.valueOf(Long.valueOf(money)).divide(new BigDecimal(100))
                    .setScale(SysContants.NUMBER.TWO,BigDecimal.ROUND_DOWN);
            return bigDecimal;
        } catch (Exception e) {
            logger.error("[trans2Y] money:{}", money, e);
            return null;
        }
    }



    public static void main(String[] args) {
        try{
            //String testMoney = "6666666";
            //System.out.println(trans2F(testMoney));
            System.out.println(changeF2Y(0));
            System.out.println(changeY2F("0.01"));
            int money =9310;
            System.out.println("分转元:"+trans2Y(money));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
