package com.xiechy.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/**
 *@ClassName AesAndroidUtil
 *@Description 与Android设备交互签名方式
 *@Author dongdong.xie
 *@Date 2020/5/28 13:44
*/
public class AesAndroidUtil {

        public static final String VIPARA = "1269571569321021";
        public static final String bm = "utf-8";



        /**
         * 字节数组转化为大写16进制字符串
         *
         * @param b
         * @return
         */
        private static String byte2HexStr(byte[] b) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < b.length; i++) {
                String s = Integer.toHexString(b[i] & 0xFF);
                if (s.length() == 1) {
                    sb.append("0");
                }

                sb.append(s.toUpperCase());
            }

            return sb.toString();
        }

        /**
         * 16进制字符串转字节数组
         *
         * @param s
         * @return
         */
        private static byte[] str2ByteArray(String s) {
            int byteArrayLength = s.length() / 2;
            byte[] b = new byte[byteArrayLength];
            for (int i = 0; i < byteArrayLength; i++) {
                byte b0 = (byte) Integer.valueOf(s.substring(i * 2, i * 2 + 2), 16)
                        .intValue();
                b[i] = b0;
            }

            return b;
        }


        /**
         * AES 加密
         *
         * @param content
         *            明文
         * @param password
         *            生成秘钥的关键字
         * @return
         */

        public static String aesEncrypt(String content, String password) {
            try {
                IvParameterSpec zeroIv = new IvParameterSpec(VIPARA.getBytes());
                SecretKeySpec key = new SecretKeySpec(password.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
                byte[] encryptedData = cipher.doFinal(content.getBytes(bm));

                return Base64.encode(encryptedData);
//			return byte2HexStr(encryptedData);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        /**
         * AES 解密
         *
         * @param content
         *            密文
         * @param password
         *            生成秘钥的关键字
         * @return
         */

        public static String aesDecrypt(String content, String password) {
            try {
                byte[] byteMi = Base64.decode(content);
//			byte[] byteMi=	str2ByteArray(content);
                IvParameterSpec zeroIv = new IvParameterSpec(VIPARA.getBytes());
                SecretKeySpec key = new SecretKeySpec(password.getBytes(), "AES");
                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
                byte[] decryptedData = cipher.doFinal(byteMi);
                return new String(decryptedData, "utf-8");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }



    public static void main(String[] args) throws Exception {

        String content="{\"closingTime\":\"2020-05-27 17:00:00\",\"licensePlateNumber\":\"沪EN1234\",\"sign\":\"D056E1133A95E1C6E45B8D4A9E1811D0\",\"trains\":\"10\",\"workTime\":\"2020-05-27 15:36:00\"}";
        String password="93177f2581864c94";
        String jiamiresult=AesAndroidUtil.aesEncrypt(content, password);
        System.out.println("tag"+"加密结果"+jiamiresult);
        String jiemi= AesAndroidUtil.aesDecrypt(jiamiresult, password);
        System.out.println("tag"+"解密结果"+jiemi);

    }

}