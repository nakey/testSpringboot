package com.xiechy.util;

import java.util.UUID;

/**
 *@ClassName UuidUtil
 *@Description 获取UUID
 *@Author dongdong.xie
 *@Date 2020/6/22 14:23
*/
public class UuidUtil {

    /**
      * @Description 获取UUID
      * @Author dongdong.xie
      * @Date  2020/6/22 14:23
      * @param 
      * @return 
     */
    public static String getUid(){
        String uuid = UUID.randomUUID().toString().replaceAll("-","");
        return uuid;
    }



}
