/*
package com.xiechy.util;

import cn.llc.common.enums.SysEnum;
import cn.llc.order.vo.DDRebotNoticeVo;

import java.util.ArrayList;
import java.util.List;

*/
/**
 * @ClassName DataUtil
 * @Description 数据工具类
 * @Author ymy
 * @Date 2020/11/23 18:59
 *//*

public class DataUtil {

    public static List<Integer> addOrderStatusGroup() {
        List<Integer> orderStatusGroup = new ArrayList<>();
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_TICKETED_FAIL.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.HANG_ORDER.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_REFUND_ING.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_REFUNDED.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_REFUND_ERROR.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_TICKET.getCode());
        orderStatusGroup.add(SysEnum.OrderStatusEnum.ORDER_TICKETED.getCode());
        return orderStatusGroup;
    }

    */
/**
      * @Description 组装钉钉机器人对象
      * @Author ymy
      * @Date  2020/12/16 13:41
      * @param content  orderNoticCode
      * @return java.lang.Object
     *//*

    public static DDRebotNoticeVo addDDRebotObject(String content, String rebotCOde) {
        DDRebotNoticeVo ddRebotNoticeVo = new DDRebotNoticeVo();
        ddRebotNoticeVo.setContent(content);
        ddRebotNoticeVo.setDdRebotCode(rebotCOde);
        return ddRebotNoticeVo;
    }
}
*/
