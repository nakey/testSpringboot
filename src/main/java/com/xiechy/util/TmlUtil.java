package com.xiechy.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

public class TmlUtil {

    private static final String IMEI_TAG = "IMEI";

    /**
     * @Description 解析tmlNo
     * @Author wuhaihui
     * @Date 2020/8/20 9:45
     * @param url
     * @return java.lang.String
     */
    public static String parseTmlNo(String url) {
        //url 格式：https://xxxxxx.com/wx/{t}.do
        if (url.contains("http") && url.contains("/")) {
            try {
                url = url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("."));
                return url;
            }catch (Exception e){
                return null;
            }
        }
        return url;
    }

    /**
     * 1.0 : Z1L1E;SN:MP06193558B3562;IMEI:861230042693797;BTMAC:48E6C03F080F;SW:1418B08SIM800C24_BT
     * 4G模块： 867567042514620;L669LP0099
     *
     * @param imeiStr
     * @return
     */
    public static String parseImei(String imeiStr) {
        try {
            //兼容4G模块imei扫码
            if (StringUtils.isNotEmpty(imeiStr) && imeiStr.length() < 35) {
                String[] imeiInfos = imeiStr.split(";");
                if (imeiInfos.length > 1) {
                    return imeiInfos[0];
                }
            } else if (StringUtils.isNotEmpty(imeiStr)) {
                String[] imeiInfos = imeiStr.split(";");
                for (String item : imeiInfos) {
                    String[] itemInfos = item.split(":");
                    if (itemInfos.length > 1) {
                        if (IMEI_TAG.equals(itemInfos[0])) {
                            return itemInfos[1];
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(TmlUtil.parseTmlNo("https://xxxxxx.com/wx/1231321.do"));
    }
}
