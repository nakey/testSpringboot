package com.xiechy.util;

import java.io.Serializable;
import java.util.Comparator;

class MapKeyComparator implements Comparator<String>, Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public int compare(String str1, String str2)
  {
    return str1.compareTo(str2);
  }
}
