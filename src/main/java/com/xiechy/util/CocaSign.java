/*
package com.xiechy.util;

import cn.llc.common.enums.SysEnum;
import cn.llc.common.response.ServerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

*/
/**
 *@ClassName CocaSign
 *@Description 签名验证类
 *@Author xdd
 *@Date 2020/5/13 14:02
 *//*

public class CocaSign {
    private static final Logger logger = LoggerFactory.getLogger(CocaSign.class);

    public CocaSign() {
    }

    public static ServerResponse<Object> sign(Map<String, Object> pMap, String sign, String cocakey) {
        String result = MapSort.getQueryString(pMap) + "&key=" + cocakey;
        logger.info("sign_MD5--》result:" + result);
        String coca_sign = MD5Util.MD5Encode(result, "UTF-8");
        logger.info("sign_MD5--》coca_sign:" + coca_sign + "==sign:" + sign);
        return !coca_sign.equalsIgnoreCase(sign) ? ServerResponse.createByErrorCodeMessage(SysEnum.ResponseCodeEnum.SIGN_IS_ERROR.getCode(), SysEnum.ResponseCodeEnum.SIGN_IS_ERROR.getMsg(), "1") : ServerResponse.createBySuccess("验签成功", "0");
    }




}
*/
