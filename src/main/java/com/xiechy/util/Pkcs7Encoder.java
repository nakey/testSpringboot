package com.xiechy.util;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Security;

/**
 * @ClassName Pkcs7Encoder
 * @Description AES128 算法
 * @Author zoujiulong
 * @Date 2020/7/24 14:55
 * @Version 1.0
 **/
public class Pkcs7Encoder {

    private static final Logger logger = LoggerFactory.getLogger(Pkcs7Encoder.class);

    private static final String EncryptAlg ="AES";

    private static final String Cipher_Mode="AES/ECB/PKCS7Padding";

    private static final String Encode="UTF-8";

    public static final String PASSWORD="93177f2581864c94ba6d342f44bfe86c";

    public static String encode(String content) {
        try {
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            byte[] raw = PASSWORD.getBytes(Encode);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, EncryptAlg);
            // "算法/模式/补码方式"
            Cipher cipher = Cipher.getInstance(Cipher_Mode);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec);

            byte[] data=cipher.doFinal(content.getBytes(Encode));
            String result= Base64.encodeBase64String(data);
            return result;
        } catch (Exception e) {
            logger.error("AES加密失败：{}",e.getMessage());
            return null;
        }
    }

    public static String decode(String content){
        try {
            byte[] raw = PASSWORD.getBytes(Encode);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, EncryptAlg);
            Cipher cipher = Cipher.getInstance(Cipher_Mode);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            // 先用base64解密
            byte[] encrypted1 = new Base64().decode(content);
            try {
                byte[] original = cipher.doFinal(encrypted1);
                String originalString = new String(original,Encode);
                return originalString;
            } catch (Exception e) {
                System.out.println(e.toString());
                return null;
            }
        } catch (Exception e) {
            logger.error("AES解密失败：" ,e.getMessage());
            return null;
        }
    }

}
