package com.xiechy.util;

import cn.hutool.core.date.DateTime;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *@ClassName CocaSign
 *@Description 日期工具类
 *@Author xdd
 *@Date 2020/5/13 15:02
 */
public class DateUtil extends DateUtils {

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    public static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM",
            "HH:mm", "yyyyMMddHHmmss", "dd/MM/yyyy HH:mm", "yyyyMMdd"};

    public static final String HH_MM_SS = "HH:mm:ss";
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    /**
     * 返回日期格式
     * 非索引值内的日期格式返回 null
     * @param index 日期格式的索引
     * @return String
     */
    public static String getParsePattern(int index) {
        if (index > parsePatterns.length || index < 0) {
            return null;
        }
        return parsePatterns[index];
    }

    /**
     * 日期格式转换
     *2021-08-30T17:31:43.000+0800  ----> 2021-08-30 17:31:43
     * @param utcDateStr utc日期str
     * @return {@link String}
     * @author nakey.xie
     * @date 2022/03/22
     */
    public static String formatUTCDate(String utcDateStr) {
        DateTime dateTime = cn.hutool.core.date.DateUtil.parse(utcDateStr, cn.hutool.core.date.format.FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSSZ", TimeZone.getTimeZone("GMT+8:00")));
        return dateTime.toString();
    }


    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd）
     */
    public static String getDate() {
        return getDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String getDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date date, Object... pattern) {
        String formatDate = null;
        if (pattern != null && pattern.length > 0) {
            formatDate = DateFormatUtils.format(date, pattern[0].toString());
        } else {
            formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime() {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String getDateTime() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     */
    public static String getWeek() {
        return formatDate(new Date(), "E");
    }

    /**
     * 日期型字符串转化为日期 格式
     * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm",
     * "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
     */
    public static Date parseDate(Object str) {
        if (str == null) {
            return null;
        }
        try {
            return parseDate(str.toString(), parsePatterns);
        } catch (ParseException e) {
            logger.warn("failed to parseDate， message: " + e.getMessage());
            return null;
        }
    }

    /**
     * 获取过去的天数
     *
     * @param date date
     * @return 过去的天数
     */
    public static long pastDays(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (24 * 60 * 60 * 1000);
    }

    /** 比较两个时间相差分钟 */
    public static long pastMinute(Date startTime, Date endTime) {
        long minute = 0;
        long millisecond = endTime.getTime() - startTime.getTime();
        minute = millisecond / (60 * 1000);
        return minute;
    }

    /**
     * 获取过去的小时
     *
     * @param date date
     * @return 过去的小时
     */
    public static long pastHour(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (60 * 60 * 1000);
    }

    /**
     * 获取过去的分钟
     *
     * @param date date
     * @return 过去的分钟
     */
    public static long pastMinutes(Date date) {
        long t = System.currentTimeMillis() - date.getTime();
        return t / (60 * 1000);
    }

    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis timeMillis
     * @return 时间字符串（天,时:分:秒.毫秒）
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = (timeMillis / (60 * 60 * 1000) - day * 24);
        long min = ((timeMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long sss = (timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 获取两个日期之间的天数
     *
     * @param before before
     * @param after  after
     * @return 两个日期之间的天数
     */
    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
    }

    /**
     * @Description 获取2个日期直接的天数
     * @Author xcy
     * @Date 2020/10/20 16:07
     * @param begin
     * @param end
     * @return long
     */
    public static long getBetweenTwoDate(Date begin, Date end) {
        return ChronoUnit.DAYS.between(begin.toInstant(), end.toInstant());
    }

    /**
     * 字符串转为日期
     *
     * @param date    字符串
     * @param pattern 模式
     * @return Date
     */
    public static Date formatStr2Date(String date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            logger.warn("failed to formatStr2Date， message: " + e.getMessage());
            return null;
        }
    }


    /**
     * @param dateStr
     * @param beforPattern MMDDhhmmss
     * @param afterPattern yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String formatStr2Date(String dateStr, String beforPattern, String afterPattern) {
        Date date = null;
        String dateStrPluse = formatDate(new Date(), "yyyy") + dateStr;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy" + beforPattern);
        try {
            date = formatter.parse(dateStrPluse);
        } catch (ParseException e) {
            logger.warn("failed to formatStr2Date,message:{}", e.getMessage());
            date = new Date();
        }
        return formatDate(date, afterPattern);
    }

    /**
     * 字符串转换日期
     *
     * @param strDate 字符串
     * @return Date
     */
    public static Date strToDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
    }

    public static Date add(final Date date, final int calendarField, final int amount) {
        if (date == null) {
            throw new IllegalArgumentException("The date must not be null");
        }
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }

    public static Date addMilliseconds(final Date date, final int amount) {
        return add(date, Calendar.MILLISECOND, amount);
    }

    public static Date addHours(final Date date, final int amount) {
        return add(date, Calendar.HOUR_OF_DAY, amount);
    }

    public static int getMonthInterval(Date from, Date to) {
        Calendar f = Calendar.getInstance();
        f.setTime(from);
        Calendar t = Calendar.getInstance();
        t.setTime(to);
        int year = t.get(1) - f.get(1);
        int month = t.get(2) - f.get(2);
        return year * 12 + month;
    }

    public static Date getDateForZeroTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        Date todayZero = calendar.getTime();
        return todayZero;
    }

    public static Date getDateForEndTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 59);
        Date todayZero = calendar.getTime();
        return todayZero;
    }

    public static String getStrForZeroTime(Date date) {
        return formatDate(getDateForZeroTime(date), "yyyy-MM-dd HH:mm:ss");
    }

    public static String getStrForEndTime(Date date) {
        return formatDate(getDateForEndTime(date), "yyyy-MM-dd HH:mm:ss");
    }

    public static Date getFirstDayOfMonth(Date batchDate) {
        Calendar c = Calendar.getInstance();
        c.setTime(batchDate);
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        return c.getTime();
    }

    /**
     * 判断两个日期是否在同一天
     *
     * @param date1
     * @param date2
     * @return
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(date1);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date2);
            if (cal1 != null && cal2 != null) {
                return cal1.get(0) == cal2.get(0) && cal1.get(1) == cal2.get(1) && cal1.get(6) == cal2.get(6);
            }
            return false;
        } else {
            return false;
        }
    }

    /**
     * @Description 上前天
     * @Author xyy
     * @Date 2020/8/18 9:51
     * @param
     * @return java.util.Date
     */
    public static Date getBeforeYesterdayDateZeroPoints() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) - 3, 0, 0, 0);
        return calendar.getTime();
    }

    /**
     * @return
     * @Description: 获取昨天零点
     * @Author:wuzhiming
     * @Date:2019/12/24
     * @Param
     */
    public static Date getYesterdayDateZeroPoints() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)-1, 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }



    /**
     * @return
     * @Description: 获取当天零点
     * @Author:wuzhiming
     * @Date:2019/12/24
     * @Param
     */
    public static Date getNowDateZeroPoints() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }


    /**
     * @return
     * @Description: 获取第二天零点
     * @Author:wuzhiming
     * @Date:2019/12/24
     * @Param
     */
    public static Date nextDayZeroPoints(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        return addDays(calendar.getTime(), 1);
    }


    /**
     * @return
     * @Description: 获取本周的第一天
     * @Author:wuzhiming
     * @Date:2019/12/24
     * @Param
     */
    public static Date getWeekStart() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.WEEK_OF_MONTH, 0);
        cal.set(Calendar.DAY_OF_WEEK, 2);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }


    /**
     * @return
     * @Description: 获取本月第一天
     * @Author:wuzhiming
     * @Date:2019/12/24
     * @Param
     */
    public static Date getMonthStart() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     *  * @Description 获得指定日期的后日期
     *  * @Author xdd
     *  * @Date  
     *  * @param 
     *  * @return 
     *  
     */
    public static Date getDayAfter(Date date, String format, int days, int hour, int min, int second) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + days);
        c.add(Calendar.HOUR, hour);
        c.add(Calendar.MINUTE, min);
        c.add(Calendar.SECOND, second);
        String dayAfterStr = new SimpleDateFormat(format).format(c.getTime());
        Date dayAfter = null;
        try {
            dayAfter = new SimpleDateFormat(format).parse(dayAfterStr);
        } catch (ParseException e) {
            logger.error("格式化异常", e);
        }
        return dayAfter;
    }

    /**
     *  * @Description 获取指定 天 时 分 秒 日期
     *  * @Author xdd
     *  * @Date  
     *  * @param 
     *  * @return 
     *  
     */
    public static Date getSpecifiedDay(int day, int hour, int min, int sec) {
        Date DateA = getDayAfter(new Date(), "yyyy-MM-dd", day, 0, 0, 0);
        Date DateB = getDayAfter(DateA, "yyyy-MM-dd HH:mm:ss", 0, hour, min, sec);
        return DateB;
    }

    public static String getDateYmdhms() {
        return getDate("yyyyMMddHHmmss");
    }

    /**
     * @Description: 格式化日期格式
     * @Author: ZYL
     * @Date: 2020/4/9 20:15
     */
    public static String getDateFormat(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    /**
     * 分钟++
     * CreateTime 2019年7月26日 下午2:45:29
     * {Author} Rocky
     * @param updateDate
     * @param minute
     * @return
     */
    public static Date afterSomeMinute(Date updateDate, int minute) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(updateDate);
        calendar.add(Calendar.MINUTE, minute);
        Date afterDate = calendar.getTime();
        return dateTime(dateTime(afterDate));
    }

    public static String dateTime(Date updated) {
        return FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(updated);
    }

    public static Date dateTime(String dateAsStr) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateAsStr);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @Description 获取前一天日期
     * @Author dongdong.xie
     * @Date 2020/7/16 16:00
     * @param
     * @return
     */
    public static String lastDayDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return DateUtil.formatDate(calendar.getTime(), parsePatterns[0]);
    }


    /**
     * @Description 获取指定天数日期
     * @Author dongdong.xie
     * @Date 2020/7/16 16:00
     * @param
     * @return
     */
    public static String getDayByIndex(int day) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, day);
        return DateUtil.formatDate(calendar.getTime(), parsePatterns[0]);
    }

    /***
     * 获取指定 时间后的 日期
     * @param time
     * @param parsePatterns
     * @return
     */
    public static String getDayByTime(int time, Date date, String parsePatterns) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, time);
        return DateUtil.formatDate(calendar.getTime(), parsePatterns);
    }

    public static String lastDayDate(String parsePatterns) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        return DateUtil.formatDate(calendar.getTime(), parsePatterns);
    }

    /**
     * 方法表述: 分钟转小时
     * @Author zoujiulong
     * @Date 11:47 2020/8/6
     * @param       date
     * @return java.lang.String
     */
    public static String getHour(String date) {
        //分钟
        Integer dt = Integer.parseInt(date);
        if (dt < 60) {
            return dt + "分钟";
        }
        int hour = Math.round(dt / 60);
        int minute = Math.round(dt - (hour * 60));
        return hour + "小时" + (minute == 0 ? "" : minute + "分钟");
    }

    /**
     * 方法表述: 获取昨天的开始时间
     * @Author zoujiulong
     * @Date 13:39 2020/8/17
     * @param
     * @return java.lang.String
     */
    public static String getYesterday() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        return yesterday.concat(" 00:00:00");
    }

    /**
     * 方法表述: 获取昨天的日期
     * @Author zoujiulong
     * @Date 13:39 2020/8/17
     * @param
     * @return java.lang.String
     */
    public static String getYesterDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String yesterday = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        return yesterday;
    }

    /**
     * 方法表述: 判断时间是否在时间段内
     * @Author zoujiulong
     * @Date 22:05 2020/8/18
     * @param       nowTime
     * @param       beginTime
     * @param       endTime
     * @return boolean  1:未开始  2:进行中  3:已结束
     */
    public static Integer belongCalendar(Date nowTime, Date beginTime, Date endTime) {
        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(beginTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        if (date.before(begin)) {
            return 1;
        }
        if (date.after(begin) && date.before(end)) {
            return 2;
        } else {
            return 3;
        }
    }

    /**
     * 功能描述: 获取年份
     * @Param: [date]
     * @Return: int
     * @Author: yaomaoyang
     * @Date: 2020/8/18 16:04
     */
    public static int getYear(Date date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        int year = ca.get(Calendar.YEAR);//年份数值
        return year;
    }

    /**
     * 功能描述: 获取年份后两位
     * @Param: [date]
     * @Return: int
     * @Author: yaomaoyang
     * @Date: 2020/8/18 16:04
     */
    public static String getYearLastTwo(Date date) {
        int year = getYear(date);
        String value = String.valueOf(year);
        return value.substring(value.length() - 2, value.length());
    }

    /**
     * 功能描述: 获取月份
     * @Param: [date]
     * @Return: int
     * @Author: yaomaoyang
     * @Date: 2020/8/18 16:04
     */
    public static String getmonth(Date date) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(date);
        // 年份数值
        int month = ca.get(Calendar.MONTH) + 1;
        return month >= 10 ? String.valueOf(month) : "0" + month;
    }

    /**
     * 方法表述: 是否为当天24h内
     * @Author zoujiulong
     * @Date 16:10 2020/9/10
     * @param       inputJudgeDate
     * @return boolean
     */
    public static boolean isToday(Date inputJudgeDate) {
        boolean flag = false;
        //获取当前系统时间
        long longDate = System.currentTimeMillis();
        Date nowDate = new Date(longDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //定义每天的24h时间范围
        Date paseBeginTime = null;
        try {
            paseBeginTime = dateFormat.parse(getNowByAfterTwentyFour());
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        if (inputJudgeDate.after(nowDate) && inputJudgeDate.before(paseBeginTime)) {
            flag = true;
        }
        return flag;
    }

    /**
     * 方法表述: 获取当前时间后24小时的时间
     * @Author zoujiulong
     * @Date 16:21 2020/9/10
     * @param
     * @return java.lang.String
     */
    public static String getNowByAfterTwentyFour() {
        //时间格式化
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        Date date = new Date();
        calendar.setTime(date);
        String time = "";
        //当前时间循环24小时减下去
        for (int i = 0; i < 24; i++) {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);
            time = sdf.format(calendar.getTime());
        }
        return time;
    }

    public static void main(String[] args) {
        //测试时间1
        //String testTimeOne = "2020-09-10 17:25:11";
        //测试时间2
        //String testTimeTwo = "2020-09-11 17:09:51";

        String testTimeOne = "2020-09-10 17:10:50";
        Date dateTest = DateUtil.formatStr2Date(testTimeOne,"yyyy-MM-dd HH:mm:ss");
        System.out.println("-----------");
        Date zero0 = DateUtil.getDateForZeroTime(dateTest);
        System.out.println("getDateForZeroTime:"+zero0.toString());
        //Date zero1 = DateUtil.getDateZeroPoints(dateTest);
        //System.out.println("getDateZeroPoints:"+zero1.toString());
        //Date endTime = DateUtil.getDateFinalPoints(dateTest);
        //System.out.println("getDateFinalPoints:"+endTime.toString());
        Date endTime1 = DateUtil.getDateForEndTime(dateTest);
        System.out.println("getDateForEndTime:"+endTime1.toString());
        System.out.println("-----------");
        System.out.println(DateUtil.formatDateTime(DateUtil.lastDayDate(new Date())));
//        String testTimeTwo = "2020-09-11 23:59:59";
//        //时间格式化
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date psTestTimeOne = null;
//        Date psTestTimeTwo = null;
//        try {
//            psTestTimeOne = format.parse(testTimeOne);
//            psTestTimeTwo = format.parse(testTimeTwo);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        boolean timeOneIsToday = isToday(psTestTimeOne);
//        boolean timeTwoIsToday = isToday(psTestTimeTwo);
//        System.out.println("测试时间输出为true，则处于当天24h范围内，false反之。");
//        System.out.println("测试时间一："+timeOneIsToday);
//        System.out.println("测试时间二："+timeTwoIsToday);
//        //测试相差天数
//        long days = getBetweenTwoDate(psTestTimeOne,psTestTimeTwo);
//        System.out.println("相差天数:"+days);

    }


    /**
     * @Description 分钟转毫秒
     * @Author ymy
     * @Date 2020/10/20 14:43
     * @param minute
     * @return java.lang.Long
     */
    public static Long minuteToMills(Integer minute) {
        return minute * 60l * 1000l;
    }

    /**
     * @Description 获取日期的上一天
     * @Author wuhaihui
     * @Date 2020/10/22 9:30
     * @param executeDate
     * @return java.util.Date
     */
    public static Date lastDayDate(Date executeDate) {
        Calendar ca = Calendar.getInstance();
        ca.setTime(executeDate);
        ca.add(Calendar.DATE, -1);
        return ca.getTime();
    }

    /**
     * @Description 毫秒转时间  String类型
     * @Author ymy
     * @Date 2020/10/20 14:43
     * @param mills
     * @return java.lang.Long
     */
    public static String millsToDateString(Long mills) {
        Date date = new Date();
        date.setTime(mills);
        return formatDateTime(date);
    }

}