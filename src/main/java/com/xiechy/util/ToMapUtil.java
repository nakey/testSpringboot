package com.xiechy.util;

import cn.llc.common.contants.SysContants;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ToMapUtil
 * @Description 转map
 * @Author zoujiulong
 * @Date 2020/6/16 14:11
 * @Version 1.0
 **/
public class ToMapUtil {

    public static <T> Map<String, Object> toMap(T bean) {
        if (bean instanceof Map) {
            return (Map<String, Object>)bean;
        }
        BeanWrapper beanWrapper = new BeanWrapperImpl(bean);
        Map<String, Object> map = new HashMap<String, Object>();
        PropertyDescriptor[] pds = beanWrapper.getPropertyDescriptors();
        for (PropertyDescriptor pd : pds) {
            if (!"class".equals(pd.getName())) {
                if(SysContants.CLASS_DATE.equals(pd.getPropertyType().getName()) && beanWrapper.getPropertyValue(pd.getName()) != null){
                    Date propertyValue = (Date) beanWrapper.getPropertyValue(pd.getName());
                    map.put(pd.getName(),DateUtil.formatDateTime(propertyValue));
                }else {
                    map.put(pd.getName(),beanWrapper.getPropertyValue(pd.getName()));
                }
            }
        }
        return map;
    }

}
