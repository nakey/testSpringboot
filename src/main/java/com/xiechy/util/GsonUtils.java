package com.xiechy.util;


import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * 
* @ClassName: GsonUtils  
* @Description: Json数据转换工具类  
* @author Paul  
* @date 2017年9月14日 
* 
* update 
* @author xdd
* @data 2018-10-15 16:44 
*
 */
public class GsonUtils {

	private static Gson gson = null;

    private static Gson dateGson = null;

    static {  
        if (gson == null) {  
            gson = new Gson();  
        }
        if(dateGson == null){
            dateGson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd hh:mm:ss")
                    .create();
        }
    }  
  
    /**
     * 
    * @Title: gsonString
    * @Description: 将object对象转成json字符串 
    * @author Paul  
    * @date 2017年9月14日 
    *  @param obj
    *  @return String
    * @throws
     */
    public static String gsonString(Object obj) {
        String json = null;  
        if (null != gson) {  
        	json = gson.toJson(obj);  
        }  
        return json;  
    }

    /**
     *
     * @Title: gsonStringFormatDate
     * @Description: 将object对象转成json字符串(并格式化日期)
     * @author Alice
     * @date 2019年6月14日
     *  @param obj
     *  @return String
     * @throws
     */
    public static String gsonStringFormatDate(Object obj) {
        String json = null;
        if (null != dateGson) {
            json = dateGson.toJson(obj);
        }
        return json;
    }


    /**
     * 
    * @Title: gsonToBean
    * @Description: 将json字符串转成泛型bean 
    * @author Paul  
    * @date 2017年9月14日 
    *  @param json
    *  @param cls
    *  @return T
    * @throws
     */
    public static <T> T gsonToBean(String json, Class<T> cls) {
        T t = null;  
        if (null != gson) {  
            t = gson.fromJson(json, cls);  
        }  
        return t;  
    }  
    
    /**
     * 
    * @Title: jsonToList  
    * @Description: json字符串转List
    * @author Paul  
    * @date 2017年9月14日 
    *  @param json
    *  @param cls
    *  @return List<T>
    * @throws
     */
    public static <T> List<T> jsonToList(String json, Class<T> cls) {  
        Gson gson = new Gson();  
        List<T> list = new ArrayList<T>();  
        JsonArray array = new JsonParser().parse(json).getAsJsonArray();
        for(final JsonElement elem : array){  
            list.add(gson.fromJson(elem, cls));  
        }  
        return list;  
    }  
    
    /**
     * 
    * @Title: gsonToListMaps
    * @Description: json字符串转成list中有map的集合
    * @author Paul  
    * @date 2017年9月14日 
    *  @param json
    *  @return List<Map<String,T>>
    * @throws
     */
    public static <T> List<Map<String, T>> gsonToListMaps(String json) {
        List<Map<String, T>> list = null;  
        if (gson != null) {  
            list = gson.fromJson(json,new TypeToken<List<Map<String, T>>>() {}.getType());
        }  
        return list;  
    }  
    
    /**
     * 
    * @Title: gsonToMaps
    * @Description: json字符串转Map对象
    * @author Paul  
    * @date 2017年9月14日 
    *  @param json
    *  @return Map<String,T>
    * @throws
     */
    public static <T> Map<String, T> gsonToMaps(String json) {
        Map<String, T> map = null;  
        if (gson != null) {  
            map = gson.fromJson(json, new TypeToken<Map<String, T>>() {}.getType());  
        }  
        return map;  
    }

    /**
     *
     * @Title: GsonToMaps
     * @Description: json字符串转Map对象
     * @author Paul
     * @date 2017年9月14日
     *  @param json
     *  @return Map<String,T>
     * @throws
     */
    public static <T> Map<String, T> GsonToMaps(String json) {
        Map<String, T> map = null;
        if (gson != null) {
            map = gson.fromJson(json, new TypeToken<Map<String, T>>() {}.getType());
        }
        return map;
    }

    /**
     *
     * @Title: GsonString
     * @Description: 将object对象转成json字符串
     * @author Paul
     * @date 2017年9月14日
     *  @param obj
     *  @return String
     * @throws
     */
    public static String GsonString(Object obj) {
        String json = null;
        if (null != gson) {
            json = gson.toJson(obj);
        }
        return json;
    }



}
