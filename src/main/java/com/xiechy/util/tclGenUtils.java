package com.xiechy.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.generator.config.ConstVal;

import com.xiechy.entity.ColumnEntity;
import com.xiechy.entity.TableEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.*;
import java.util.*;

/**
 * @author nakey.xie
 * @version v1.0
 * @Description 自动生成工具类
 * @date 2022-1-21 16:52:59
 */
@Slf4j
public class tclGenUtils {

    /**
     * SQL连接
     */
    private static Connection connection;

    /**
     * 代码生成主方法
     */
    public static void main(String[] args) {
        generatorCode("com.company.project", "test-module", "app_info");
    }

    /**
     * 获取所有模板
     *
     * @return {@link List}<{@link String}>
     * @author nakey.xie
     * @date 2022/01/21
     */
    public static List<String> getTemplates() {
        List<String> templates = new ArrayList<>();
        templates.add("auroraTemplate/Entity.java.vm");
        templates.add("auroraTemplate/Mapper.java.vm");
        templates.add("auroraTemplate/Service.java.vm");
        templates.add("auroraTemplate/ServiceImpl.java.vm");
        templates.add("auroraTemplate/Controller.java.vm");
        return templates;
    }

    /**
     * @Description 自动生成代码
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2022/1/21 15:02
     * @param packageName 包名
     * @param module 模块名
     * @param tableNames 表名集合
     * @return void
    */
    private static void generatorCode(String packageName, String module, String... tableNames) {
        log.debug("==========================准备生成代码...==========================");
        handlerDataSource();
        for (String tableName : tableNames) {
            //查询表信息
            Map<String, String> table = queryTable(tableName);
            //查询列信息
            List<ColumnEntity> columns = queryColumns(tableName);
            //生成代码
            generatorByTable(table, columns, packageName, module);
        }
        log.debug("==========================代码生成完成！！！==========================");
        closeConnect();
    }



    /**
     * 查询列
     *
     * @param tableName 表名
     * @return {@link List}<{@link ColumnEntity}>
     * @author nakey.xie
     * @date 2022/01/21
     */
    private static List<ColumnEntity> queryColumns(String tableName) {
        List<ColumnEntity> columnEntities = new ArrayList<>();
        String sql = "select column_name columnName, data_type dataType, " +
                "column_comment columnComment, column_key columnKey, extra " +
                "from information_schema.columns " +
                " where table_name = ? and table_schema = (select database()) " +
                "order by ordinal_position";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, tableName);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                ColumnEntity columnEntity = new ColumnEntity();
                columnEntity.setColumnName(set.getString("columnName"));
                columnEntity.setDataType(set.getString("dataType"));
                columnEntity.setComments(set.getString("columnComment"));
                columnEntity.setColumnKey(set.getString("columnKey"));
                columnEntity.setExtra(set.getString("extra"));
                columnEntities.add(columnEntity);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if (CollectionUtil.isEmpty(columnEntities)) {
            throw new RuntimeException("查询列数据为空!");
        }
        return columnEntities;
    }


    /**
     * 查询表
     *
     * @param tableName 表名
     * @return {@link Map}<{@link String}, {@link String}>
     * @author nakey.xie
     * @date 2022/01/21
     */
    private static Map<String, String> queryTable(String tableName) {
        Map<String, String> tableMap = new HashMap<>();
        String sql = "select table_name tableName, table_comment tableComment " +
                "from information_schema.tables " +
                "where table_schema = (select database()) and table_name = ? ";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, tableName);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                String table = set.getString("tableName");
                String tableComment = set.getString("tableComment");
                tableMap.put("tableName", table);
                tableMap.put("tableComment",tableComment);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        if (tableMap.isEmpty()) {
            throw new RuntimeException("查询表数据为空!");
        }
        return tableMap;
    }



    /**
     * 根据表与列信息生成代码
     * @param table       表格
     * @param columns     列
     * @param packageName 包名
     * @param module      模块
     * @author nakey.xie
     * @date 2022/01/21
     * @date 2022/01/21
     */
    public static void generatorByTable(Map<String, String> table, List<ColumnEntity> columns, String packageName, String module) {
        //配置信息
        Configuration config = getConfig();
        boolean hasBigDecimal = false;
        //表信息
        TableEntity tableEntity = new TableEntity();
        tableEntity.setTableName(table.get("tableName"));
        tableEntity.setComments(table.get("tableComment"));
        //表名转换成Java类名
        String className = tableToJava(tableEntity.getTableName(), config.getStringArray("tablePrefix"));
        tableEntity.setClassName(className);
        tableEntity.setClassname(StringUtils.uncapitalize(className));
        tableEntity.setClassNameLower(className.toLowerCase());

        //列信息
        List<ColumnEntity> columsList = new ArrayList<>();
        //忽略一些字段不用生成（在beseEntity里已经有了）
        List<String> ignoreColumns = Arrays.asList(
                new String[]{"create_time","update_time","create_by","update_by","remark","deleted","id"});
        for (ColumnEntity columnEntity : columns) {
            //列名转换成Java属性名
            String attrName = columnToJava(columnEntity.getColumnName());
            columnEntity.setAttrName(attrName);
            columnEntity.setAttrname(StringUtils.uncapitalize(attrName));

            //列的数据类型，转换成Java类型
            String attrType = config.getString(columnEntity.getDataType(), "unknowType");
            attrType = coverEnumType(attrType, attrName, config, tableEntity);
            columnEntity.setAttrType(attrType);
            if (!hasBigDecimal && "BigDecimal".equals(attrType)) {
                hasBigDecimal = true;
            }
            //是否主键
            if ("PRI".equalsIgnoreCase(columnEntity.getColumnKey()) && tableEntity.getPk() == null) {
                tableEntity.setPk(columnEntity);
            }

            if(ignoreColumns.contains(columnEntity.getColumnName())){
                continue;
            }
            columsList.add(columnEntity);
        }
        tableEntity.setColumns(columsList);

        //没主键，则第一个字段为主键
        if (tableEntity.getPk() == null) {
            tableEntity.setPk(tableEntity.getColumns().get(0));
        }

        //设置velocity资源加载器
        Properties prop = new Properties();
        prop.put("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(prop);
        //包名
        packageName = StringUtils.isEmpty(packageName)?config.getString("package") : packageName;
        //封装模板数据
        Map<String, Object> map = new HashMap<>(15);
        map.put("tableName", tableEntity.getTableName());
        map.put("comments", tableEntity.getComments());
        map.put("pk", tableEntity.getPk());
        map.put("className", tableEntity.getClassName());
        map.put("classname", tableEntity.getClassname());
        map.put("pathName", tableEntity.getClassname().toLowerCase());
        map.put("columns", tableEntity.getColumns());
        map.put("classNameLower", tableEntity.getClassNameLower());
        map.put("enums", tableEntity.getEnums());
        map.put("hasBigDecimal", hasBigDecimal);
        map.put("package", packageName);
        map.put("author", config.getString("author"));
        map.put("datetime", DateUtil.now());
        map.put("identity", IdWorker.getId());
        map.put("addId", IdWorker.getId());
        map.put("updateId", IdWorker.getId());
        map.put("deleteId", IdWorker.getId());
        map.put("selectId", IdWorker.getId());
        VelocityContext context = new VelocityContext(map);

        String projectPath = System.getProperty("user.dir");
        projectPath = StringUtils.isEmpty(module) ? projectPath : projectPath + File.separator + module;
        String outputDir = projectPath + "/src/main/java";

        //获取模板列表
        List<String> templates = getTemplates();
        for (String template : templates) {
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            String path = getFileName(template, tableEntity.getClassName(), packageName, outputDir);
            try {
                if(isCreate(path)){
                    writer(tpl,tpl.getName(),path,context);
                }
            } catch (Exception e) {
                log.error("生成失败",e);
                throw new RuntimeException("生成模板失败，表名：" + tableEntity.getTableName());
            }
        }
    }


    /**
     * @Description 文件写出
     * @Author  nakey.xie
     * @Date  2022/1/20 16:00
     * @param template 模板
     * @param templatePath 模板名
     * @param outputFile 文件路径
     * @param context 内容
     * @return void
    */
    public static void writer(Template template, String templatePath, String outputFile, VelocityContext context) throws Exception {
        try (FileOutputStream fos = new FileOutputStream(outputFile);
             OutputStreamWriter ow = new OutputStreamWriter(fos, ConstVal.UTF8);
             BufferedWriter writer = new BufferedWriter(ow)) {
            template.merge(context, writer);
        }
        log.debug("模板:" + templatePath + ";  文件:" + outputFile);
    }


    /**
     * 检测文件是否存在
     *
     * @return 文件是否存在
     */
    protected static boolean isCreate( String filePath) {
        // 全局判断【默认】
        File file = new File(filePath);
        boolean exist = file.exists();
        if (!exist) {
            file.getParentFile().mkdirs();
        }
        return !exist;
    }

    /**
     * @Description 如果字段为Integer可能为枚举类型，如果找到对应枚举类型，则转换成枚举
     * @Author R&D-VAL SZ nakey.xie
     * @Date  2021/11/8 11:54
     * @param attrType
     * @return java.lang.String
    */
    private static String coverEnumType(String attrType, String attrName, Configuration config, TableEntity tableEntity) {
        if (!"Integer".equals(attrType)) {
            return attrType;
        }
        String path = config.getString("enumPath");
        if (StringUtils.isEmpty(path)) {
            return attrType;
        }
        String enumName = StrUtil.upperFirst(attrName);
        String pathEnumName = path+enumName;
        Class clazz = null;
        try {
            clazz = Class.forName(pathEnumName);
        } catch (ClassNotFoundException e) {
            return attrType;
        }
        if (clazz.isEnum()) {
            attrType = enumName;
            List<String> enums = tableEntity.getEnums();
            if(enums == null){
                enums = new ArrayList<>();
            }
            enums.add(pathEnumName);
        }
        return attrType;
    }


    /**
     * 列名转换成Java属性名
     */
    public static String columnToJava(String field) {
        String[] fields = field.split("_");
        StringBuilder sbuilder = new StringBuilder(fields[0]);
        for (int i = 1; i < fields.length; i++) {
            char[] cs = fields[i].toCharArray();
            cs[0] -= 32;
            sbuilder.append(String.valueOf(cs));
        }
        return sbuilder.toString().substring(0, 1).toUpperCase() + sbuilder.toString().substring(1);
    }


    /**
     * 表名转换成Java类名
     */
    public static String tableToJava(String tableName, String[] tablePrefixArray) {
        tableName = tableName.toLowerCase();
        if (null != tablePrefixArray && tablePrefixArray.length > 0) {
            for (String tablePrefix : tablePrefixArray) {
                tablePrefix = tablePrefix.toLowerCase();
                tableName = tableName.replace(tablePrefix, "");
            }
        }
        return columnToJava(tableName);
    }

    /**
     * 获取配置信息
     */
    public static Configuration getConfig() {
        try {
            return new PropertiesConfiguration("tclGenerator.properties");
        } catch (ConfigurationException e) {
            throw new RuntimeException("获取配置文件失败");
        }
    }

    /**
     * 处理数据源配置
     *
     */
    private static void handlerDataSource() {
        Configuration config = getConfig();
        String driverName = config.getString("driver");
        String url = config.getString("url");
        String userName = config.getString("userName");
        String password = config.getString("password");
        try {
            Class.forName(driverName);
            connection = DriverManager.getConnection(url, userName, password);
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void closeConnect() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 获取文件名
     */
    public static String getFileName(String template, String className, String packageName, String outputDir) {
        String resultPth = null;
        String packagePath = outputDir + File.separator;
        String dirPath = null;
        if (StringUtils.isNotBlank(packageName)) {
            packagePath += packageName.replace(".", File.separator) + File.separator;
        }

        if (template.contains("Entity.java.vm")) {
            dirPath = packagePath + "entity";
            resultPth = packagePath + "entity" + File.separator + className + ".java";
        }

        if (template.contains("Mapper.java.vm")) {
            dirPath = packagePath + "mapper";
            resultPth = packagePath + "mapper" + File.separator + className + "Mapper.java";
        }

        if (template.contains("Service.java.vm")) {
            dirPath = packagePath + "service";
            resultPth = packagePath + "service" + File.separator + "I" + className + "Service.java";
        }

        if (template.contains("ServiceImpl.java.vm")) {
            dirPath = packagePath + "service" + File.separator + "impl";
            resultPth = packagePath + "service" + File.separator + "impl" + File.separator + className + "ServiceImpl.java";
        }

        if (template.contains("Controller.java.vm")) {
            dirPath = packagePath + "controller";
            resultPth = packagePath + "controller" + File.separator + className + "Controller.java";
        }
        File dir = new File(dirPath);
        if (!dir.exists()) {
            boolean result = dir.mkdirs();
            if (result) {
                log.debug("创建目录： [" + resultPth + "]");
            }
        }
        return resultPth;
    }

}
