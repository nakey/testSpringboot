package com.xiechy.util;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author xdd
 * @date 2018/9/13 17:10
 * @description map排序工具
 */

public class MapSort {
	public static Map<String, Object> sortMapByKey(Map<String, Object> map) {
		if ((map == null) || (map.isEmpty())) {
			return null;
		}
		Map sortMap = new TreeMap(new MapKeyComparator());
		sortMap.putAll(map);
		return sortMap;
	}

	public static String getQueryString(Map<String, Object> pMap) {
		StringBuffer queryStr = new StringBuffer("");
		try {
			Map resultMap = sortMapByKey(pMap);
			Iterator iter = resultMap.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String)iter.next();
				if(StringUtils.isNotBlank(resultMap.get(key).toString())) {
					if ("".equals(queryStr.toString())) {
						queryStr.append(key + "=" + resultMap.get(key).toString());
				    }else {
				        queryStr.append("&" + key + "=" + resultMap.get(key).toString());
				    }
				}
			}
		} catch (Exception localException) {
			localException.printStackTrace();
		}
		return queryStr.toString();
	}

	/**
	 * 将url参数转换成map
	 * @param param aa=11&bb=22&cc=33
	 * @return
	 */
	public static Map<String, Object> getUrlParams(String param) {
		Map<String, Object> map = new HashMap<String, Object>(0);
		if (StringUtils.isBlank(param)) {
			return map;
		}
		String[] params = param.split("&");
		for (int i = 0; i < params.length; i++) {
			String[] p = params[i].split("=");
			if (p.length == 2) {
				map.put(p[0], p[1]);
			}
		}
		return map;
	}
	
	/**
	 * 验证签名拼接
	 * @version:
	 * @author: wjz 
	 * @date: 2018年12月18日 下午4:10:24
	 * @param
	 */
	public static String getQueryString2(Map<String,Object> pMap) {
		StringBuffer queryStr = new StringBuffer("");
		try {
			Map resultMap = sortMapByKey2(pMap);
			Iterator iter = resultMap.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String)iter.next();
				if(StringUtils.isNotBlank(String.valueOf(resultMap.get(key)))) {
					if (queryStr.length() == 0) {
						queryStr.append(key + "=" + resultMap.get(key).toString());
				    }else {
				    	queryStr.append("&" + key + "=" + resultMap.get(key).toString());
				    }
				}
			}
		} catch (Exception localException) {
		}
		return queryStr.toString();
	}
	
	/**
	 * 字符串排序
	 * @version:
	 * @author: wjz 
	 * @date: 2018年12月18日 下午4:15:59
	 * @param
	 */
	public static Map<String, Object> sortMapByKey2(Map<String, Object> map) {
		if ((map == null) || (map.isEmpty())) {
			return null;
		}
		Map sortMap = new TreeMap(new MapKeyComparator());
		sortMap.putAll(map);
		return sortMap;
	}

	/****
	 * list 转 Map<Object,Map<Object,String>>
	 * @param list 按照模板值顺序放
	 * @param type 0公众号 1小程序
	 * @return
	 */
	public static Map<Object,Map<Object,String>> data(List<String> list, int type){
		Map<Object,Map<Object,String>> maps = new LinkedHashMap<>();
		Map<Object,String> childmaps = null;
		for(int i=0;i<list.size();i++){
			if(type==0 && i==0){
				childmaps = new LinkedHashMap<>();
				childmaps.put("value",list.get(i));
				childmaps.put("color","#173177");
				maps.put("first",childmaps);
				continue;
			}
			if(type==0 && i==(list.size()-1)){
				childmaps = new LinkedHashMap<>();
				childmaps.put("value",list.get(i));
				childmaps.put("color","#173177");
				maps.put("remark",childmaps);
				continue;
			}
			childmaps = new LinkedHashMap<>();
			childmaps.put("value",list.get(i));
			childmaps.put("color","#173177");
			if(type==0){
				maps.put("keyword"+i,childmaps);
			}else{
				maps.put("keyword"+(i+1),childmaps);
			}
		}
		return maps;
	}
}
