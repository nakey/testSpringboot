package com.xiechy.constant;

/**
 * @ClassName SensorConstant
 * @Description
 * @author wuhaihui
 * @date 2020/9/17 15:49
 */
public class SensorConstant {
    /**
     * 电机状态
     */
    public enum MotorStatus {

        STOP(0, "停止"),

        POSITIVE_DIRECTION(1, "正方向"),

        OPPOSITE_DIRECTION(2, "反方向"),

        BLOCK(3, "堵转"),

        ERROR(-1, "故障");

        private int status;

        private String desc;

        MotorStatus(int status, String desc) {
            this.status = status;
            this.desc = desc;
        }

        public int getStatus() {
            return status;
        }

        public String getDesc() {
            return desc;
        }
    }

    /**
     * 电机状态
     */
    public enum SensorStatus {

        NONE(0, "没有"),

        HAVE(1, "有"),

        ERROR(-1, "故障");

        private int status;

        private String desc;

        SensorStatus(int status, String desc) {
            this.status = status;
            this.desc = desc;
        }

        public int getStatus() {
            return status;
        }

        public String getDesc() {
            return desc;
        }
    }
}
