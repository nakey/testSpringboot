package cn.llc.common.contants;

import java.math.BigDecimal;

/**
 *@ClassName SysContants
 *@Description 系统常量类
 *@Author xdd
 *@Date 2020/5/13 14:02
 */
public class SysContants {


    /**
     * 系统用户缓存
     */
    public static final String USER_NAME_SESSION_ID_PREFIX = "sys:login:UsernameSessionId:";

    /**
     * 系统用户登录失败次数缓存key
     */
    public  static final String LOCK_PREFIX = "sys:login:LoginFailCount:";

    /**
     * 工作状态 OPERATING_STATE
     */
    public static final String OPERATING_STATE = "OPERATING_STATE";
    /**
     * 设备状态 TML_STATUS
     */
    public static final String TML_STATUS = "TML_STATUS";
    /**
     * 设备类型 TML_KIND
     */
    public static final String TML_KIND = "TML_KIND";

    /**
     * 操作人: 系统
     */
    public static final String OPERATOR_SYSTEM = "system";
    /**
     * 每包数量
     */
    public static final Integer LOTTERY_PACKAGE_NUM = 120;

    /**
     * 小黑挂了，redis--key
     */
    public static final String XIAO_HEI_OUTAGE = "xiaoheiOutage";
    /**
     * 小黑挂了,短信提醒 redis--key
     */
    public static final String XIAO_HEI_RESTART = "xiaoheiRestart";

    /**
     * @Description 设备心跳，多少时间一次（单位：秒）
     */
    public static final Integer THE_HEARTBEAT = 200;

    /**
     * @Description 设备key
     */
    public static final String KEY_TML_PREFIX = "KEY_TML_PREFIX:";

    /**
     * @Description 设备静音请求路径
     */
    public static final String TML_SERVER = "/tml-server/soundSetting";

    /**
     * @Description 带兑奖数据KEY
     */
    public static final String PRIZE_RESULT_IS_INIT = "PRIZE_RESULT_IS_INIT";

    /**
     * RID:620852548819214336,result:2,amount:0.0,errorMsg:该票已在2020-09-09 13:26:24时间进行兑奖，兑奖者为3151151491，中奖金额：5元。
     * 重复兑奖核验
     */
    public static final String IS_LLC = "时间进行兑奖，兑奖者为3151151491，";

    /**
     * @Description 小黑宕机提醒角色key
     */
    public static final String OUTAGE_ALERT_TASK = "OUTAGE_ALERT_TASK";

    /**
     * @Description 启动小黑编号key
     */
    public static final String START_PRIZE_NO = "START_PRIZE_NO";

    /**
     * @Description 提醒路路彩购彩角色key
     */
    public static final String REMIND_PURCHASE_TASK = "REMIND_PURCHASE_TASK";

    /**
     * @Description 宕机
     */
    public static final String DOWNTIME = "DOWNTIME";

    /**
     * @Description 重启
     */
    public static final String RESTART = "RESTART";

    /**
     * @Description 时间
     */
    public static final String CLASS_DATE = "java.util.Date";

    /**
      * @Description 服务商维度每天心跳数量记录
      * @Author xyy
      * @Date  2020/10/21 15:50
      * @param null
      * @return
     */
    public static final String KEY_Z_PROVIDER_HEAR_BEAT_COUNT_PREFIX = "KEY_Z_PROVIDER_HEAR_BEAT_COUNT_PREFIX:";

    /**
     * @Description 系统单位
     */
    public static class SYSTEM_UNIT {
        //包
        public static String PACKAGE = "包";
        //张
        public static String SHEET = "张";
        //台
        public static String PIECE = "台";

    }

    public static class VW_CODE {
        //与大众交互密钥
        public static String KEY = "93177f2581864c94";
        //与小黑交互密钥
        public static String PRIZE_KEY = "A3d928f388F19cFe";

    }

    /**
     * 彩票分类key
     */
    public static class LOTTERY_KIND {

        public static String KEY = "LOTTERY_KIND";

    }

    /**
     * 司机补贴限额key
     */
    public static class AGENT_SUBSIDY_LIMIT {

        public static String KEY = "AGENT_SUBSIDY_LIMIT";

    }

    /**
     * banner展示数量key
     */
    public static class BANNER_LOAD_NUM {

        //乘客端
        public static String KEY = "BANNER_LOAD_NUM";

        //司机端
        public static String AGENT_KEY ="AGENT_BANNER_LOAD_NUM";

    }

    /**
     * 同中同得活动开关key
     */
    public static class REWARD_AGENT_ACTIVITY_FLAG {

        public static String KEY = "REWARD_AGENT_ACTIVITY_FLAG";
        //开
        public static String ON = "ON";
        //关
        public static String OFF = "OFF";

    }

    /***
     * 数字类常量
     */
    public static class NUMBER {

        public static int ZERO = 0;

        public static int ONE = 1;

        public static int TWO = 2;

        public static int THREE = 3;

        public static int FOUR = 4;

        public static int TEN = 10;

        public static int TWENTY = 20;

        public static int ONE_HUNDRED = 100;

    }

    /**
     * 正则表达式
     */
    public static class Regex {

        /**
         * 手机号码
         */
        public static final String PHONE_NUMBER_PATTERN = "^(?:(?:\\+|00)86)?1(?:(?:3[\\d])|(?:4[5-7|9])|(?:5[0-3|5-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\\d])|(?:9[1|8|9]))\\d{8}$";

        /**
         * 车牌号
         */
        public static final String PLATE_NO_PATTERN = "^(?:[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z]{1}[A-HJ-NP-Z]{1}(?:(?:[0-9]{5}[DF])|(?:[DF](?:[A-HJ-NP-Z0-9])[0-9]{4})))$|(?:[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z]{1}[A-Z]{1}[A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9 挂学警港澳]{1})$";

        /**
         * 非特殊字符
         */
        public static final String NOT_SPECIAL = "^(?!_)(?!.*?_$)[a-zA-Z0-9_\\u4e00-\\u9fa5]+$";
    }

    /**
     * redisKey
     */
    public static class Redis {
        /**
         * 司机注册redisKey VERIFY_CODE_AGENT_REGISTER_:phone
         */
        public static final String VERIFY_CODE_AGENT_REGISTER_PREFIX = "VERIFY_CODE_AGENT_REGISTER:";
        /**
         * 运营端变更手机号redisKey VERIFY_CODE_ADMIN_REGISTER_PREFIX:phone
         */
        public static final String VERIFY_CODE_ADMIN_REGISTER_PREFIX = "VERIFY_CODE_ADMIN_REGISTER_PREFIX:";
        /**
         * lock 司机获取验证码锁redisKey
         */
        public static final String LOCK_AGENT_GET_CODE_PREFIX = "LOCK_AGENT_GET_CODE_PREFIX:";

        /**
         * lock 运营端获取验证码锁redisKey
         */
        public static final String ADMIN_LOCK_AGENT_GET_CODE_PREFIX = "ADMIN_LOCK_AGENT_GET_CODE_PREFIX:";

        /**
         * 手机号获取验证码次数限制key
         */
        public static final String VERIFY_CODE_PHONE_COUNT_PREFIX = "VERIFY_CODE_PHONE_COUNT:";
        /**
         * lock 彩票回收
         */
        public static final String LOCK_LOTTERY_RECYCLE_PREFIX = "LOCK_LOTTERY_RECYCLE:";

        /**
         * 设备出票
         */
        public static final String TML_PRINT_PREFIX = "TML_PRINT_PREFIX:";

        /**
         * lock 司机提现加锁
         */
        public static final String LOCK_AGENT_CASH_PREFIX = "LOCK_AGENT_CASH:";

        /**
         * lock 新司机审核加锁
         */
        public static final String LOCK_AGENT_AUDIT_PREFIX = "LOCK_AGENT_AUDIT:";

        /**
         * lock 司机绑车/车绑司机加锁
         */
        public static final String LOCK_AGENT_BIND_CAR_PREFIX = "LOCK_AGENT_BIND_CAR:";

        /**
         * lock 兑奖订单参与活动加锁
         */
        public static final String LOCK_ACTIVITY_PREFIX = "LOCK_ACTIVITY:";

        /**
         * lock 用户额度账户加锁
         */
        public static final String LOCK_USER_QUOTA_ACCOUNT = "LOCK_USER_QUOTA_ACCOUNT:";

        /**
         * lock 确认收货加锁
         */
        public static final String RECEIPT_PREFIX = "RECEIPT:";

        /**
         * 出票锁
         */
        public static final String PRINT_LOCK = "PRINT_LOCK:";


        /**
         * 下发指令zset
         */
        public static final String Z_PRINT = "Z_PRINT";
        /**
         * 下发指令zset
         */
        public static final String Z_HAND_PRINT = "Z_HAND_PRINT";
        /**
         * 反馈队列
         */
        public static final String PRINT_ACK_QUEUE = "PRINT_ACK_QUEUE";

        /**
         * 创建设备列表的状态
         */
        public static final String CREATE_DEVICE_STATUS = "CREATE_DEVICE_STATUS";

        /**
         * 创建设备批次锁  key
         */
        public static final String CREATE_DEVICE_LOCK = "create_device_lock";
        /**
         *  订单超时队列
         */
        public static final String ORDER_TIMEOUT_QUEUE = "ORDER_TIMEOUT_QUEUE";

        /**
         *  公众号openId集合 key
         */
        public static final String OPENID_LIST = "OPENID_LIST";

        /**
         *  在线设备set key
         */
        public static final String ONLINE_SET_TML_KEY = "ONLINE_SET_TML_KEY";

        /**
         *  设备状态询问key
         */
        public static final String S_PRE_PRINT = "S_PRE_PRINT";
        /**
         *  挂单统计
         */
        public static final String HANG_ORDER_COUNT = "HANG_ORDER_COUNT";

        /**
         *  出票失败统计
         */
        public static final String TICKETED_FAIL_COUNT = "TICKETED_FAIL_COUNT";

        /**
         *  挂单转成出票失败的订单统计
         */
        public static final String HAND_CHANGE_FAIL_COUNT = "HAND_CHANGE_FAIL_COUNT";

        /**
         * 设备升级锁key
         */
        public static final String KEY_TML_UPGRADE = "KEY_TML_UPGRADE:";
        /**
         * 司机自动审核每天限制笔数
         */
        public static final String AUTO_AUDIT_DAY_COUNT_LIMIT = "auto_audit_day_count_limit";

        /**
         *  司机自动审核每天限制笔数key的有效时间，单位：秒
         */
        public static final Long AUTO_AUDIT_DAY_COUNT_LIMIT_CACHE_TIME = 24 * 60 *60L;

        /**
         * @Description 设备每日异常类型key
         */
        public static final String TML_EXCEPTION_KEY = "tml_exception_key:";

        /**
         * @Description 按天统计设备异常key
         */
        public static final String TML_EXCEPTION_DATE = "tml_exception_date";


    }

    /***
     * 常用字符类型
     */
    public static class STR {

        /***
         * 默认IP
         */
        public static String DEFAULT_IP = "127.0.0.1";
        /**
         * 返回成功结果
         */
        public static final String SUCCESS = "success";

        /**
         * 返回失败结果
         */
        public static final String FAIL = "fail";

        /**
         * 兑奖备注
         */
        public static final String PRIZE_REMARK = "兑奖提现";

        /***
         * 包号 未上车
         */
        public static final String PRIZE_NO_IN_PARK = "未销售激活";

        /**
         * 横杠
         */
        public static final String RUNG = "-";

        /**
         * 司机提现
         */
        public static final String AGENT_CASH_REMARK = "司机账户提现到微信余额";


    }

    /***
     * 排序
     */
    public static class SORT {

        /**
         * 兑奖备注
         */
        public static final String DESC = "desc";

        /**
         * 兑奖备注
         */
        public static final String ASC = "asc";

    }

    /***
     * 排序字段
     */
    public static class SORT_FIELD {

        /**
         * 创建时间
         */
        public static final String CREATE_DATE = "create_date";

        /**
         * 退款时间
         */
        public static final String REFUND_DATE = "refund_date";

        /***
         * 兑奖时间
         */
        public static final String CHECK_DATE = "check_date";

        /***
         * 支付时间
         */
        public static final String PAY_DATE = "pay_date";

        /***
         * 发放时间
         */
        public static final String CASH_DATE = "cash_date";

    }

    /*
     * @Description 车辆最大绑定司机数
     * @Author xcy
     * @Date  2020/6/24 10:12
     */
    public static final int MAX_BING_AGENT = 2;

    /*
     * @Description 定义“全国”的城市编号(banner)
     * @Author xcy
     * @Date  22020-7-24 09:29:06
     */
    public static final String ALL_CITY = "allCityNo";

    /*
     * @Description 司机账户更改类型
     * @Author xcy
     * @Date  2020-7-21 14:01:26
     */
    public static class AccountChangeType {

        /***
         * 加可提现金额
         */
        public static final String ADD_MONEY = "ADD";

        /***
         * 减可提现金额
         */
        public static final String SUB_MONEY = "SUB";
    }

    /***
     * 微信相关配置
     */
    public static class WeiChat {

        public static final String WX_AUTH_URL = "https://api.weixin.qq.com/sns/jscode2session?appid=APPID&secret=SECRET&js_code=JSCODE&grant_type=authorization_code";

        public static final String WX_GRANTTYPE = "authorization_code";

    }

    /***
     * token标记
     */
    public static class Token {
        public static final String TOKEN_TYPE = "VMS_SCO_AUTHTOKEN:";

    }

    /**
     * 短信key
     */
    public static class SMS {
        /**
         * 司机注册短信模板
         */
        public static final String SMS_AGENT_RES_PASS = "SMS_AGENT_RES_PASS";

        /**
         * 运营端变更手机号短信模板
         */
        public static final String SMS_ADMIN_CHANGE_PHONE_RES_PASS = "SMS_ADMIN_CHANGE_PHONE_RES_PASS";

        /**
         * 提醒路路彩购彩
         */
        public static final String SMS_REMIND_PURCHASE = "SMS_REMIND_PURCHASE";

        /**
         * pc端创建用户成功发送密码
         */
        public static final String SMS_CREATE_USER_PASS = "SMS_CREATE_USER_PASS";
        /**
         * pc端重置用户成功发送密码
         */
        public static final String SMS_RES_PASS = "SMS_RES_PASS";

        /**
         * 小黑宕机提醒
         */
        public static final String SMS_OUTAGE_ALERT = "SMS_OUTAGE_ALERT";

        /**
         * 提醒司机用户使用优惠劵
         */
        public static final String SMS_NOTICE_USE_COUPON = "SMS_NOTICE_USE_COUPON";

        /**
         * 提醒运营人员进行手动退款
         */
        public static final String SMS_NOTICE_AUTO_REFUND = "SMS_NOTICE_AUTO_REFUND";
    }

    /**
     * 公众号模板编号
     */
    public static class WX_TEMPLATE_ID {
        /**
         * 推送司机奖励金到账模板
         */
        //public static final String AGENT_COMMISSION = "RfThXwGWhhDFxXRHn-MT5xWuTJicNuBFsZIDLsTnV1s";
        public static final String AGENT_COMMISSION = "FWmXM01biJrMcxcL4iLXFlwx0d8HdTxtzmkxF_1NGeU";

    }

    /**
     * 微信消息路由页面
     */
    public static class WX_ROUTE_PAGE {
        /**
         * 小程序收益页面
         */
        public static final String AGENT_COMMISSION_PAGE = "pages/authorization/index?path=/pages/commission/index";

        /**
         * 小程序用户端主页
         */
        public static final String HOME_PAGE = "pages/index/index";

    }

    /**
     * 系统参数
     */
    public static class DICT {


        /***
         *
         * 体彩中心账户佣金
         */
        public static String SPORTSLOTTERYCENTERACCOUNT = "SPORTSLOTTERYCENTERACCOUNT";
        /**
         * 公司基本户佣金
         */
        public static final String BASICCOMPANYACCOUNT = "BASICCOMPANYACCOUNT";
        /**
         * 消息发送总入口开关配置(短信，邮箱，站内信)（ON:开启；OFF:关闭）
         */
        public static final String MESSAGE_SENDING = "MESSAGE_SENDING";
        /**
         * 短信是否发送开关配置（ON:开启；OFF:关闭）
         */
        public static final String IS_SEND = "IS_SEND";
        /**
         * 短信（ON:开启）
         */
        public static final String ON = "ON";
        /**
         * 短信（OFF:关闭）
         */
        public static final String OFF = "OFF";

        /**
         * 司机自动审核
         */
        public static final String AGENT_AUTO_AUDIT = "AGENT_AUTO_AUDIT";

        /**
         * 第三方服务域名
         */
        public static final String THIRD_PART_DOMAIN = "third_part_domain";

        /**
         * 自动退款失败需要通知的手机号码
         */
        public static final String AUTO_REFUND_FAIL_NOTIC_PHONE = "auto_refund_fail_notic_phone";
        /**
         * 最大重试次数
         */
        public static final String UPGRADE_RETRY_COUNT = "UPGRADE_RETRY_COUNT";

        /**
         * 司机提现自动审核状态，是否开启
         */
        public static final String AUTO_AUDIT_AGENT_WITHDRAW_STATUS = "auto_audit_agent_Withdraw";

        /**
         * 司机提现自动审核单笔限额
         */
        public static final String AUTO_AUDIT_AGENT_WITHDRAW_SINGLE_LIMIT = "auto_audit_agent_withdraw_single_limit";

        /**
         * 司机提现自动审核每日笔数限制
         */
        public static final String AUTO_AUDIT_AGENT_WITHDRAW_DAY_NUM_LIMIT = "auto_audit_agent_withdraw_day_num_limit";

        /**
         * 司机提现自动审核每日笔数限制
         */
        public static final Integer AUTO_AUDIT_AGENT_WITHDRAW_DEFAULT_STATUS = 0;

    }

    /**
     * 路由API
     */
    public static class ROUTER_API {
        public static final String GET_ONLINE_TERMINAL = "/v1/api/online-tml";
    }

    /**
     * 每日自动退款最高上线
     */
    public static final BigDecimal MAX_REFUND_LIMIT = new BigDecimal("1000000");

    /**
     * 每日自动退款默认金额
     */
    public static final BigDecimal DEFAULT_REFUND_LIMIT = new BigDecimal("1000");

    /**
     * 发送钉钉群消息的接口
     */
    public static final String DD_AUTO_REFUND_NOTIC_INTERFACE = "/dd/sendMsg";

    /**
     * 导出最大数据
     */
    public static final Integer EXPORT_MAX_LIMIT = 10000;


    /**
     * @Description 钉钉机器人code
     * @Author ymy
     * @Date  2020/10/21 10:04
     */
    public class DDRebotCode{

        /**
         * 订单相关钉钉通知
         */
        public static final String ORDER_NOTIC_CODE = "order_notic_code";

        /**
         * 退款失败警告通知
         */
        public static final String REFUND_FAIL_WARN_NOTICE_CODE = "refund_fail_warn_notice_code";

        /**
         * 每日简报钉钉通知
         */
        public static final String DAILY_DATA_CODE ="daily_data_code";

        /**
         * 司机提现审核通知
         */
        public static final String AGENT_WITHDRAW_AUDIT_CODE ="agent_withdraw_audit_code";

        /**
         * 彩民兑奖打款失败
         */
        public static final String PRIZE_FAIL_NOTICE_CODE ="prize_fail_notice_code";

        /**
         * 大金额奖金喜报通知
         */
        public static final String BIG_BONUS_GOODS_NEWS_CODE ="big_bonus_goods_news_code";

        /**
         * 设备异常警告
         */
        public static final String TML_EXCEPTION_WARN_CODE ="tml_exception_warn_code";

    }

}

